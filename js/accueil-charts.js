"use strict";

function setInfos() {
    document.getElementById('data_infos').innerHTML = `<p style="text-align: justify;">En page d'accueil, données relatives aux résultats des tests virologiques COVID-19 SI-DEP, en déploiement depuis le 13 mai 2020. Visualisation en proportion de population testée (pourcentage établi à partir du nombre cumulé de tests journaliers réalisés). Sur France entière, région entière et département.
        Description complète sur <a href="https://www.data.gouv.fr/fr/datasets/donnees-relatives-aux-resultats-des-tests-virologiques-covid-19/">data.gouv.fr</a><br>
        Pour les onglets "Régions" et "Départements" : le nombre quotidien de personnes nouvellement hospitalisées, en réanimation, décédées ou retours à domicile.<br>
        Pour les départements des indicateurs couleurs d'activité épidémique COVID-19 (zone rouge, orange ou verte).<br>
        Sur tous les graphiques, les bandes grisées pour marquer les samedis & dimanches.<br>
        Les jeux de données sont accessibles sur data.gouv.fr et mis à jour quotidiennement aux alentours de 19h00 par <a href="https://www.data.gouv.fr/fr/organizations/sante-publique-france/" title="Sante Publique France" target="_blank">Sante Publique France</a>.<br>
        Ce prototyme d'application n'est prévu qu'à fin de tests.
        Merci de signaler toutes erreurs, omissions, bugs ou incohérences dans la répresentation des données.<br>
        Par ailleurs, certains fichiers pouvant comporter des anomalies du fait des difficultés de collecte des données.
        Description complète et rapport d'erreurs sur le site <a href="https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/" target="_blank" title="data.gouv.fr"> data.gouv.fr</a>
        </p>`
}

var getDepNom = (selected, label) => (selected === "dep" && label !== "région entière") ?
    '<span>(' + label + ') ' + app.postcodes[label].dep_nom + '</span>' :
    '<span>' + label + '</span>';

function valuesDashboard(date, selected, label, dict, dates, type) {
    var last7days = Object.entries(dict)
        .sort((a, b) => a[0] - b[0])
        .slice(-7)
        .map(d => Object.entries(d[1]).filter(d => d[0] == label));

    // cumul 7 jours glissants
    var last7days_t = d3.sum(last7days.map(d => d3.sum(d.map(d => d[1].t))));
    var last7days_p = d3.sum(last7days.map(d => d3.sum(d.map(d => d[1].p))));

    var last7days_mean = last7days_p / last7days_t;

    var last7days_pop = last7days[0][0][1].pop;
    var taux_incidence = localeFR.format(".2f")((last7days_p * 100000) / last7days_pop);

    var values = dict[date][label];
    var mean = values.t_nombre / (dates.length / 7);

    // var tableHeaders = selected !== "dep" ? ["région", "taux de positivité <sup>(4)</sup>", "taux de positivité <sup>(6)</sup>"] : ["département", "taux de positivité <sup>(4)</sup>", "taux de positivité <sup>(6)</sup>"];

    // var sortedCasPositifs = Object.entries(dict[date]).sort((a, b) => b[1].cas_percent - a[1].cas_percent).map(d => [getDepNom(selected, d[0]), localeFR.format(".2%")(d[1].cas_percent), localeFR.format(".2%")(d[1].p_nombre / d[1].pop)]);

    // var tableCasPositifs = createTable(tableHeaders, sortedCasPositifs);

    var getLabelMinMax = (label) => selected === "dep" && app.postcodes[label] !== undefined ? '(' + label + ') ' + app.postcodes[label].dep_nom : label;

    var getValuesMinMax = (prop, val) => {
        var value = val === "min" ? Infinity : -Infinity,
            label,
            pop,
            flag,
            key;
        for (var key in dict[date]) {
            if (key !== "France entière" && key !== "région entière") {
                flag = val === "min" ? dict[date][key][prop] < value : dict[date][key][prop] > value;
                if (flag) {
                    value = dict[date][key][prop];
                    label = getLabelMinMax(key);
                    pop = dict[date][key].pop;
                }
            }
        }
        return {
            value: value,
            label: label,
            pop: pop
        }
    }

    var dataDash = '<p><b>Au ' + moment(date, "YYYY-MM-DD").format("LL") +
        ' (' + getDepNom(selected, label) + ')' + '</b></p><ul class="w3-ul list-current">';

    var dataDash = '<p><b>Au ' + moment(date, "YYYY-MM-DD").format("LL") +
        ' (' + getDepNom(selected, label) + ')' + '</b></p><ul class="w3-ul list-current">';

    dataDash += type !== "indic" ? '<li>Tests effectués (ce jour) : <b>' + convertToMillions(values.t) + '</b></li>' : '';
    dataDash += type === "dash" ? '<li>Tests effectués (7 jours glissants) : <b>' + convertToMillions(Math.round(last7days_t)) + '</b></li>' : '';
    dataDash += type !== "indic" ? '<li>Tests effectués (par semaine) : <b>' + convertToMillions(Math.round(mean)) + '</b></li>' : '';
    dataDash += type !== "indic" ? '<li>Tests effectués (cumul) : <b>' + convertToMillions(values.t_nombre) + ' </b><sup>(1)</sup></li>' : '';

    dataDash += type !== "indic" ? '<br><li>Cas positifs (ce jour) : <b>' + convertToMillions(values.p) + '</b> </li>' : '';
    dataDash += type !== "indic" ? '<li>Cas positifs (cumul) : <b>' + convertToMillions(values.p_nombre) + ' </b></li>' : '';
    dataDash += type !== "indic" ? '<li>Taux de positivité (ce jour) : <b>' + localeFR.format(".2%")(values.p / values.t) + '</b> <sup>(4)</sup></li>' : '';

    dataDash += type === "dash" ? '<li>Taux de positivité (7 jours glissants) : <b>' + localeFR.format(".2%")(last7days_mean) + '</b> <sup>(4)</sup></li>' : '';

    dataDash += type === "dash" ?
        '<br><li>Taux de positivité le plus élevé (ce jour) pour ' + getValuesMinMax("cas_percent", "max").label +
        ' : <b>' + localeFR.format(".2%")(getValuesMinMax("cas_percent", "max").value) + '</b></li>' : '';

    dataDash += type !== "indic" ?
        '<br><li>Taux d\'incidence pour ' + label + ' : <b>' + taux_incidence + '</b> <sup>(6)</sup></li>' : '';

    dataDash += type === "dash" ? '<br><li><b>' + localeFR.format(".2%")(values.t_nombre / values.pop) + '</b> <sup>(2)</sup> de population testée soit <b>' + convertToMillions(values.t_nombre) + '</b> personnes sur <b>' + convertToMillions(values.pop) + '</b> <sup>(3)</sup> ' + label + '</li>' :
        type === "current" ?
        '<br><li><b>' + localeFR.format(".2%")(values.t_nombre / values.pop) + '</b> <sup>(2)</sup> de population testée soit <b>' + convertToMillions(values.t_nombre) + '</b> personnes sur <b>' + convertToMillions(values.pop) + '</b> <sup>(3)</sup> ' + label + '</li>' : '';

    // dataDash += type === "indic" ? tableCasPositifs : '';

    dataDash += '</ul>';

    return dataDash;
}

function getDepts(data) {
    var depts = {};

    var nestedData = d3.nest()
        .key(d => d.region)
        .rollup(v => {
            return {
                depts: [...new Set(v.map(d => d.dep))]
            }
        })
        .entries(data);

    nestedData.forEach(d => {
        depts[d.key] = d.value.depts
    })
    return depts;
}

function notesExpand(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}

document.getElementById("accueil_regions_select").addEventListener('change', function (e) {
    var data = app.dataComplete["dataTests"];

    document.getElementById('accueil_legend_current').innerHTML = "survolez ou touchez pour sélectionner";
    var data = (this.value === "") ? data : data.filter(d => d.region === this.value);
    var selected = (this.value === "") ? undefined : "dep";

    accueilChart(data, selected);
}, false);

function accueilChart(data, selected) {
    setInfos();

    selected = (selected !== undefined) ? selected : "region";
    var labelMean = selected == "dep" ? "région entière" : "France entière";

    var dates = [...new Set(data.map(d => d["jour"]))];
    var lastDate = dates[dates.length - 1];

    var unit;
    var field = document.querySelector('input[name=fields]:checked').value;
    var scale = document.querySelector('input[name = "scales"]:checked').value;

    var nestedData = d3.nest()
        .key(d => d.jour)
        .key(d => d[selected])
        .rollup(v => {
            return {
                pop: d3.sum(v.map(d => d.pop)),
                t_cumul: d3.sum(v.map(d => d.testsCumul)),
                t_percent: d3.sum(v.map(d => d.testsCumul)) / d3.sum(v.map(d => d.pop)),
                p_cumul: d3.sum(v.map(d => d.casCumul)),
                p_percent: d3.sum(v.map(d => d.casCumul)) / d3.sum(v.map(d => d.testsCumul)),
                t: d3.sum(v.map(d => d.t)),
                p: d3.sum(v.map(d => d.p)),
                cas_percent: d3.mean(v.map(d => d.cas_percent))
            }
        })
        .entries(data)

    var dataset = [];
    var seriesValues = {};
    var namesByPercent = {};
    var propPopFrance = {};
    var testsCumulTotal = 0;

    var getTnombreByDay = function (obj, key) {
        var total = 0;
        for (var item in obj) {
            total += obj[item][key]
        }
        return total;
    }

    var getLast7days = (dict) => dict.map(d => d.values);

    nestedData.forEach((d, index) => {
        var length = dates.length - 1;
        var end = length - index;
        var start = (index <= 6) ? 0 : index - 6;

        var date = d.key;
        var values1 = d.values;
        var population = 0;
        var t_cumul = 0;
        var p_cumul = 0;
        var row = [];
        var obj = {};

        values1.sort(function (a, b) {
            return b.value.t_percent - a.value.t_percent;
        });

        values1.forEach((d, j) => {
            var name = d.key;
            obj[name] = {
                t_nombre: d.value.t_cumul,
                t_percent: d.value.t_percent,
                pop: d.value.pop,
                p_nombre: d.value.p_cumul,
                p_percent: d.value.p_percent,
                t: d.value.t,
                p: d.value.p,
                cas_percent: d.value.cas_percent
            };
            namesByPercent[name] = d.value.t_percent;

            row.push(d.value.t_percent)

            t_cumul += d.value.t_cumul;
            p_cumul += d.value.p_cumul;
            population += d.value.pop;
            testsCumulTotal += t_cumul;
        });

        obj[labelMean] = {
            t_nombre: t_cumul,
            t_percent: (t_cumul / population),
            pop: population,
            p_nombre: p_cumul,
            p_percent: (p_cumul / population),
            t: getTnombreByDay(obj, "t"),
            p: getTnombreByDay(obj, "p"),
            cas_percent: getTnombreByDay(obj, "p") / getTnombreByDay(obj, "t")
        };

        namesByPercent[labelMean] = (t_cumul / population);

        row.push(t_cumul / population);

        propPopFrance[date] = (t_cumul / population);

        seriesValues[date] = obj;

        row.sort((a, b) => b - a);

        row.unshift(new Date(date));
        dataset.push(row);
    });

    document.getElementById('accueil_dash').innerHTML = valuesDashboard(lastDate, selected, labelMean, seriesValues, dates, "dash");

    document.getElementById('indicateurs_dash').innerHTML = valuesDashboard(lastDate, selected, labelMean, seriesValues, dates, "indic");

    var labelsSorted = Object.entries(namesByPercent).sort((a, b) => b[1] - a[1]);
    var labels = labelsSorted.map(d => d[0]);

    var last7daysByLabel = {};
    labels.forEach(label => {
        last7daysByLabel[label] = { tests: 0, positifs: 0, cas_percent: 0, pop: 0};
    })

    var getLast7days = (dict) => dict.map((d, i) => {
        for (var key in d[1]) {
            last7daysByLabel[key]["t_percent"] = d[1][key].t_percent;
            last7daysByLabel[key]["tests"] += d[1][key].t;
            last7daysByLabel[key]["positifs"] += d[1][key].p;
            last7daysByLabel[key]["cas_percent"] = d[1][key].cas_percent;
            last7daysByLabel[key]["pop"] = d[1][key].pop;
        };
    });

    getLast7days((Object.entries(seriesValues)).slice(-7));

    var tableHeaders = selected !== "dep" ? ["région", "tests <sup>(2)</sup>", "positivité <sup>(5)</sup>", "incidence<sup>(6)</sup>", "ratio <sup>(7)</sup>"] : ["département", " tests <sup>(2)</sup>", "positivité <sup>(5)</sup>", "incidence <sup>(6)</sup>", "ratio <sup>(7)</sup>"];


    var sortedCasPositifs = Object.entries(last7daysByLabel).sort((a, b) => ((b[1].positifs * 100000) / b[1].pop)/(b[1].t_percent*100) - ((a[1].positifs * 100000) / a[1].pop)/(a[1].t_percent*100))
        .map(d => [
            getDepNom(selected, d[0]),
            localeFR.format(".2%")(d[1].t_percent),
            localeFR.format(".2%")(d[1].positifs / d[1].tests),
            localeFR.format(".2f")((d[1].positifs * 100000) / d[1].pop),
            localeFR.format(".2f")(((d[1].positifs * 100000) / d[1].pop)/(d[1].t_percent*100)),
        ]);

    var tableCasPositifs = createTable(tableHeaders, sortedCasPositifs);

    document.getElementById('indicateurs_dash').innerHTML = '<p><b>Au ' + moment(lastDate, "YYYY-MM-DD").format("LL") +
        '</b> (pour trier, cliquez sur un titre de colonne)</p>' + tableCasPositifs;

    document.getElementById('accueil_dash').innerHTML = valuesDashboard(lastDate, selected, labelMean, seriesValues, dates, "dash");

    var series = {};
    labels.forEach((label, i) => {
        if (label !== labelMean) {
            series[label] = {
                plotter: barChartPlotter,
            }
        } else {
            series[label] = {
                strokeWidth: 3,
                stepPlot: true,
                fillGraph: true,
                color: "#550000"
            }
        }
    });

    var legendFormatter = function (data) {
        var getTotalSelection = function (percent, nombre) {
            var selected_region = document.getElementById("accueil_regions_select").value;
            return "";
            // selected_region !== "" ?
            //     ' => région entière ' + formatThousands(nombre) +
            //     ' tests (' + localeFR.format(".2%")(percent) + ')' :
            //     ' => France entière ' + formatThousands(nombre) +
            //     ' tests (' + localeFR.format(".2%")(percent) + ')';
        }

        var lineDiv = (color, label) => {
            return label == labelMean ?
                '<div class="dygraph-legend-line" style="border-bottom-width: 5px;"></div>' :
                '<div class="dygraph-legend-line" style="border-top: solid 2px ' + color + '; border-bottom: solid 10px ' + darkenColor(color, 0.3) + '; vertical-align: bottom;"></div>';
        };
        if (data.x == null) {

            return '<b>' + this.getLabels()[0] + ' ' +
                moment(lastDate, "YYYY-MM-DD").format("LL") + ' </b>' + getTotalSelection(seriesValues[lastDate][labelMean].t_percent, seriesValues[lastDate][labelMean].t_nombre) + '<br>' + data.series
                .map(series => lineDiv(series.color, series.labelHTML) + ' ' + getDepNom(selected, series.labelHTML) + ' : ' + '<span class="w3-round" style="padding: 2px 5px">' +
                    convertToMillions(seriesValues[lastDate][series.labelHTML].t_nombre) + ' tests <sup>(1)</sup>, ' +
                    localeFR.format(".2%")(namesByPercent[series.labelHTML]) + ' <sup>(2)</sup></span>')
                .join('<br>');
        }
        var date = moment(data.xHTML, "YYYY-MM-DD").format("YYYY-MM-DD");
        var html = '<b>' + this.getLabels()[0] + ' ' + moment(data.xHTML, "YYYY-MM-DD").format("LL") + '</b>' + getTotalSelection(seriesValues[lastDate][labelMean].t_percent, seriesValues[lastDate][labelMean].t_nombre);

        data.series.forEach(function (series) {
            if (!series.isVisible) return;
            var values = seriesValues[date][series.label];
            var mean = Math.round(values.t_nombre / (dates.length / 7));

            var labeledData = getDepNom(selected, series.label) + ' : <span class="w3-round" style="padding: 2px 5px;">' + convertToMillions(values.t_nombre) + ' tests <sup>(1)</sup>, </span> ' + series.yHTML + ' <sup>(2)</sup>';

            var labeledDataHighlighted = getDepNom(selected, series.label) + ' : ' +
                convertToMillions(values.t_nombre) + ' tests <sup>(1)</sup>, <span class="w3-round" style="padding: 2px 5px; background-color:' + series.color + '; color:white">' + series.yHTML + '</span> <sup>(2)</sup>';

            var labeledDataCurrent = valuesDashboard(date, selected, series.label, seriesValues, dates, "current")

            if (series.isHighlighted) {
                labeledData = '<b>' + labeledDataHighlighted + '</b>';
                document.getElementById('accueil_legend_current').innerHTML = labeledDataCurrent;
            }
            html += '<br>' + lineDiv(series.color, series.labelHTML) + ' ' + labeledData;
        });
        return html;
    }
    // ajout colonne ("jour") pour X axis
    labels.unshift("Au");

    var g3 = new Dygraph(
        document.getElementById("accueil_chart"),
        dataset, {
            labels: labels,
            height: Math.round(window.innerHeight * 0.80),
            highlightCircleSize: 2,
            highlightSeriesOpts: {
                strokeWidth: 2,
                strokePattern: [20, 20],
                highlightCircleSize: 6
            },
            highlightSeriesBackgroundAlpha: 0.5,
            title: '<span class="title-graph" style="font-size: 16px; font-weight: normal">(bandes grisées = weekend)</span>',
            labelsDiv: document.getElementById('accueil_legend'),
            legend: 'always',
            legendFormatter: legendFormatter,
            unhighlightCallback: function (e, x, pts, row) {
                g3.clearSelection();
            },
            underlayCallback: hightlight_WE,
            showRangeSelector: true,
            rangeSelectorPlotFillColor: 'white',
            rangeSelectorAlpha: 0.5,
            rangeSelectorPlotFillGradientColor: "#550000",
            yAxisLabelWidth: 200,
            ylabel: "tests en pourcentage par population (données brutes)",
            axes: {
                x: {
                    axisLabelFormatter: function (d, gran, opts) {
                        return moment(d).format("DD MMM"); // A REVOIR
                    },
                },
                y: {
                    valueFormatter: function (y) {
                        return unit !== 'nombre' ? localeFR.format(".2%")(y) : formatThousands(y);
                    },
                    axisLabelFormatter: function (y) {
                        return unit !== 'nombre' ? localeFR.format(".2%")(y) : formatThousands(y);
                    },
                    pixelsPerLabel: 50,
                    axisLabelWidth: 80
                }
            },
            series: series
        });
}

function openDash(tabName) {
    if (tabName == "description") {
        document.getElementById('dateFrom_title').style.display = "none";
        document.getElementById('accueil_regions_select').style.display = "none";
    } else {
        document.getElementById('dateFrom_title').style.display = "block";
        document.getElementById('accueil_regions_select').style.display = "block";
    }
    var i;
    var x = document.getElementsByClassName("dash");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    document.getElementById(tabName).style.display = "block";
}

// Fonctions graphiques
// Darken a color
function darkenColor(colorStr, opacity) {
    // Defined in dygraph-utils.js
    var color = Dygraph.toRGB_(colorStr);

    color.r = Math.floor((255 + color.r) / 2);
    color.g = Math.floor((255 + color.g) / 2);
    color.b = Math.floor((255 + color.b) / 2);
    return 'rgba(' + color.r + ',' + color.g + ',' + color.b + ',' + opacity + ')';
}

function barChartPlotter(e) {
    var ctx = e.drawingContext;
    var points = e.points;
    var y_bottom = e.dygraph.toDomYCoord(0);

    ctx.fillStyle = darkenColor(e.color, 0.3);

    // Find the minimum separation between x-values.
    // This determines the bar width.
    var min_sep = Infinity;
    for (var i = 1; i < points.length; i++) {
        var sep = points[i].canvasx - points[i - 1].canvasx;
        if (sep < min_sep) min_sep = sep;
    }
    var bar_width = Math.floor(2.0 / 3 * min_sep);

    // Do the actual plotting.
    for (var i = 0; i < points.length; i++) {
        var p = points[i];
        var center_x = p.canvasx;

        ctx.fillRect(center_x - bar_width / 2, p.canvasy,
            bar_width, y_bottom - p.canvasy);

        ctx.strokeRect(center_x - bar_width / 2, p.canvasy,
            bar_width, y_bottom - p.canvasy);
    }
}

function hightlight_WE(canvas, area, g, dygraphs) {
    canvas.fillStyle = "rgba(188, 196, 197, 0.5)";

    function highlight_period(x_start, x_end) {
        var canvas_left_x = g.toDomXCoord(x_start);
        var canvas_right_x = g.toDomXCoord(x_end);
        var canvas_width = canvas_right_x - canvas_left_x;
        canvas.fillRect(canvas_left_x, area.y, canvas_width, area.h);
    }

    var min_data_x = g.getValue(0, 0);
    var max_data_x = g.getValue(g.numRows() - 1, 0);

    // get day of week
    var d = new Date(min_data_x);
    var dow = d.getUTCDay();

    var w = min_data_x;
    // starting on Sunday is a special case
    if (dow === 0) {
        highlight_period(w, w + 12 * 3600 * 1000);
    }
    // find first saturday
    while (dow != 6) {
        w += 24 * 3600 * 1000;
        d = new Date(w);
        dow = d.getUTCDay();
    }
    // shift back 1/2 day to center highlight around the point for the day
    w -= 12 * 3600 * 1000;
    while (w < max_data_x) {
        var start_x_highlight = w;
        var end_x_highlight = w + 2 * 24 * 3600 * 1000;
        // make sure we don't try to plot outside the graph
        if (start_x_highlight < min_data_x) {
            start_x_highlight = min_data_x;
        }
        if (end_x_highlight > max_data_x) {
            end_x_highlight = max_data_x;
        }
        highlight_period(start_x_highlight, end_x_highlight);
        // calculate start of highlight for next Saturday
        w += 7 * 24 * 3600 * 1000;
    }
};

// création simple de table html pour sweetALert pvtTable
function createTable(headers, data) {
    var table = '<table id="table_taux" class="w3-table-all tableOverflow-y" style="margin:5px auto;">';
    table += '<thead><tr>';
    headers.forEach((header, i) => {
        // table += '<th onclick="sortTable(' + i + ')"><div><i class="gg-sort-az"></i>&ensp;' + header + '</div></th>';
        table += '<th onclick="sortTable(' + i + ')"><div>' + header + '</div></th>';
    });
    table += '</tr></thead>';
    table += '<tbody>';
    data.forEach(row => {
        // console.log(row.length);
        table += '<tr>';
        row.forEach((cell, j) => {
            table += '<td>' + cell + '</td>';
        })
        table += '</tr>';
    });
    table += '</tbody></table>';

    return table;
}

function sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, flag, dir, switchcount = 0;
    table = document.getElementById("table_taux");
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";
    var convertToDecimal = (value) => /\,/.test(value) ? parseFloat((value.replace(/\,/, '.'))) : parseFloat(value);
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            flag = (isNaN(parseFloat(x.innerHTML)) && isNaN(parseFloat(y.innerHTML)));
            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase() && flag) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                } else if (convertToDecimal(x.innerHTML) > convertToDecimal(y.innerHTML) && !flag) {
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase() && flag) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                } else if (convertToDecimal(x.innerHTML) < convertToDecimal(y.innerHTML) && !flag) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}