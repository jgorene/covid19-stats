"use strict";

const isIos = /ip(hone)/i.test(navigator.userAgent);
const isIpad = /ip(ad)/i.test(navigator.userAgent);
const isAndroid = /android/i.test(navigator.userAgent);
const isMobile = isIos || isAndroid || isIpad;

moment.locale('fr')

// commons with tabulator table
var localeFR = d3.formatLocale({
    decimal: ",",
    thousands: " ",
    grouping: [3]
});

var format0dec = localeFR.format(".0f");
var format2dec = localeFR.format(".2f");
var format3dec = localeFR.format(".3f");
var formatPercent = localeFR.format(".3%");
var formatExponent = localeFR.format(".2e");
var formatThousands = localeFR.format(",");

function convertToMillions(value) {
    // console.log(value);
    // Nine Zeroes for Billions
    return Math.abs(Number(value)) >= 1.0e+9
    ? localeFR.format(".3f")(Math.round((Math.abs(value) / 1.0e+9 + 0.00001) * 1000) / 1000) + " billions"

    // Six Zeroes for Millions
    : Math.abs(Number(value)) >= 1.0e+6
    ? localeFR.format(".3f")(Math.round((Math.abs(value) / 1.0e+6 + 0.00001) * 1000) / 1000) + " millions"

    : localeFR.format(",")(Math.abs(value));
}

var fr = {
    "name": "fr",
    "options": {
        "months": [
            "janvier",
            "février",
            "mars",
            "avril",
            "mai",
            "juin",
            "juillet",
            "août",
            "septembre",
            "octobre",
            "novembre",
            "décembre"
        ],
        "shortMonths": [
            "janv.",
            "févr.",
            "mars",
            "avr.",
            "mai",
            "juin",
            "juill.",
            "août",
            "sept.",
            "oct.",
            "nov.",
            "déc."
        ],
        "days": [
            "dimanche",
            "lundi",
            "mardi",
            "mercredi",
            "jeudi",
            "vendredi",
            "samedi"
        ],
        "shortDays": ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
        "toolbar": {
            "exportToSVG": "format SVG",
            "exportToPNG": "format PNG",
            "exportToCSV": "format CSV",
            "menu": "Menu",
            "selection": "Sélection",
            "selectionZoom": "Sélection et zoom",
            "zoomIn": "Zoomer",
            "zoomOut": "Dézoomer",
            "pan": "Navigation",
            "reset": "Réinitialiser le zoom"
        }
    }
}

// ferme le menu sur ipad à la sélection d'un item
function closeMenulist(el) {
    var mediaQuerie = window.matchMedia("(min-width: 49em)");
    // console.log(mediaQuerie, el);

    var setClick = () => {
        if (el !== undefined) {
            el.click();
        }
    }
    if (mediaQuerie !== undefined && mediaQuerie.matches) { // If media query matches
        var event = new Event("touchstart");
        el.addEventListener("touchstart", setClick, false);
        el.dispatchEvent(event);
        el.removeEventListener("touchstart", setClick, false);
    } else {
        setClick();
    }
}

document.getElementById("menulist").addEventListener("click", function (e) {
    var children = this.childNodes;
    if (e.target) {
        // replie le menu hamburger après choix item
        closeMenulist(document.getElementById('menu-btn')); // Call listener function at run time

        document.querySelectorAll('.page').forEach((el) => {
            var page = document.getElementById(el.id);
            // window.getComputedStyle(page, null).getPropertyValue("opacity")
            if (page.offsetHeight === 0) {
                children.forEach(child => {
                    if (child.nodeName === "LI") {
                        var classDrop = child.classList.contains('dropdown');
                        var child_a = child.querySelector("a");
                        var child_div = child.querySelector("div");
                        if (e.target.getAttribute('href') !== child_a.getAttribute("href")) {
                            child_a.classList.remove('menu-active');
                            if (classDrop) {
                                if (e.target.getAttribute('href') !== child_div.querySelector("a").getAttribute("href"))
                                    child_a.classList.remove('menu-active');

                                child_div.childNodes.forEach(child => {
                                    if (child.nodeName === "A")
                                        if (e.target.getAttribute('href') !== child.getAttribute("href"))
                                            child.classList.remove('menu-active');
                                });
                            }
                        }
                    }
                })
            } else {
                if (!e.target.classList.contains('dropdown'))
                    e.target.classList.add('menu-active');
            }
        })
    }
})
"use strict";

var createPage = function (route) {
    // var auth = /auth/.test(route) ? 'Accès avec authenfication' : 'Accès sans authentification';
    // console.log(auth);

    var mainDiv = document.getElementById('main_div');

    var divPage = document.createElement('div')
    divPage.id = route;
    divPage.className = 'page';
    // divPage.style.marginTop = '60px';
    divPage.dataset.state = "";

    var titlePage = document.createElement('H3');
    titlePage.id = route + '_title';
    titlePage.className = "w3-center";

    var divTable = document.createElement('div');
    divTable.id = route + '_div';
    divTable.className = 'w3-row';
    // divTable.style.margin = "5px auto";

    var divChartContainer = document.createElement('div');
    divChartContainer.className = "w3-col s12 l9";

    var divChart = document.createElement('div');
    divChart.id = route + '_chart';
    divChart.style.width = '98%';
    divChart.style.minWIdth = '450px';

    // div selection dates et items region ou département
    var divLegendSelection = document.createElement('div');
    divLegendSelection.id = route + '_divLegendSelection';
    divLegendSelection.className = "w3-center w3-blue-grey";
    // divLegendSelection.title = "Clic long pour faire défiler";
    divLegendSelection.style.width = "100%";
    divLegendSelection.style.fontSize = "1.2em";
    divLegendSelection.style.margin = "1em auto";

    var divBarDates = document.createElement('div');
    divBarDates.className = "w3-center"; // w3-left

    var divSelectionDates = document.createElement('div');
    divSelectionDates.className = "w3-show-inline-block"; // w3-left

    var spanSelectionDatesLeft = document.createElement('span');
    spanSelectionDatesLeft.innerHTML = '<span id="' + route +
        '_left_dates_btn" class="w3-button w3-hover-blue-grey"><i class="gg-chevron-left"></i></span>';

    var spanContainerDates = document.createElement('span');
    spanContainerDates.innerHTML = '<span id="' + route +
        '_dates_container" class="w3-button w3-hover-blue-grey" style="cursor: auto"></span>';

    var spanSelectionDatesRight = document.createElement('span');
    spanSelectionDatesRight.innerHTML = '<span id="' + route +
        '_right_dates_btn" class="w3-button w3-hover-blue-grey"><i class="gg-chevron-right"></i></span>';

    // var divBarNames = document.createElement('div');
    // divBarNames.className = "w3-center"; // w3-left

    // var divSelectionNames = document.createElement('div');
    // divSelectionNames.style.width = "400px";
    // divSelectionNames.className = "w3-show-inline-block";

    // var spanSelectionNamesLeft = document.createElement('span');
    // spanSelectionNamesLeft.innerHTML = '<span id="' + route +
    //     '_left_names_btn" class="w3-button w3-left w3-hover-blue-grey"><i class="gg-chevron-left"></i></span>';

    // var spanContainerNames = document.createElement('span');
    // spanContainerNames.innerHTML = '<span id="' + route +
    //     '_names_container" class="w3-button w3-hover-blue-grey" style="cursor: auto"></span>';

    // var spanSelectionNamesRight = document.createElement('span');
    // spanSelectionNamesRight.innerHTML = '<span id="' + route +
    //     '_right_names_btn" class="w3-button w3-right w3-hover-blue-grey"><i class="gg-chevron-right"></i></span>';


    divBarDates.appendChild(spanSelectionDatesLeft);
    divBarDates.appendChild(spanContainerDates);
    divBarDates.appendChild(spanSelectionDatesRight);
    divSelectionDates.appendChild(divBarDates);
    divLegendSelection.appendChild(divSelectionDates);

    // divBarNames.appendChild(spanSelectionNamesLeft);
    // divBarNames.appendChild(spanContainerNames);
    // divBarNames.appendChild(spanSelectionNamesRight);
    // divSelectionNames.appendChild(divBarNames);
    // divLegendSelection.appendChild(divSelectionNames);

    // divPage.appendChild(divLegendSelection);
    // ==========


    // Legend container ============
    var divLegendContainer = document.createElement('div');
    divLegendContainer.id = route + '_divLegendContainer';
    divLegendContainer.className = "w3-col s12 l3";

    var divLegendPanel = document.createElement('div');
    divLegendPanel.id = route + '_divLegendPanel';
    divLegendPanel.className = "w3-panel";
    divLegendPanel.style.pointerEvents = "none";

    var divLegendPermanent = document.createElement('div');
    divLegendPermanent.id = route + '_divLegendPermanent';
    divLegendPermanent.className = "w3-panel";
    divLegendPermanent.style.pointerEvents = "none";

    var divLegendDateX = document.createElement('div');
    divLegendDateX.id = route + '_legend_dateX';
    divLegendDateX.className = "w3-panel w3-leftbar w3-border-blue-grey";
    divLegendDateX.style.position = "absolute";
    divLegendDateX.style.top = "130px";
    divLegendDateX.style.left = "120px"

    var divLegendTotal = document.createElement('div');
    divLegendTotal.id = route + '_legend_total';
    divLegendTotal.className = "w3-panel w3-leftbar w3-border-blue-grey";
    divLegendTotal.style.position = "absolute";
    divLegendTotal.style.top = "160px";
    divLegendTotal.style.left = "120px";

    var divLegendSelected = document.createElement('div');
    divLegendSelected.id = route + '_legend_selected';
    divLegendSelected.className = "w3-panel w3-leftbar w3-border-blue-grey";
    divLegendSelected.style.position = "absolute";
    divLegendSelected.style.top = "200px";
    divLegendSelected.style.left = "120px";

    var divLegendPopulation = document.createElement('div');
    divLegendPopulation.id = route + '_legend_population';
    divLegendPopulation.className = "w3-panel w3-leftbar w3-border-blue-grey";
    divLegendPopulation.style.position = "absolute";
    divLegendPopulation.style.top = "230px";
    divLegendPopulation.style.left = "120px";
    divLegendPopulation.style.display = "none";

    var divLegendRatioHosp = document.createElement('div');
    divLegendRatioHosp.id = route + '_legend_ratioHosp';
    divLegendRatioHosp.className = "w3-panel w3-leftbar w3-border-blue-grey";
    divLegendRatioHosp.style.position = "absolute";
    divLegendRatioHosp.style.top = "270px";
    divLegendRatioHosp.style.left = "120px";
    divLegendRatioHosp.style.display = "none";

    var divLegendSelect = document.createElement('select');
    divLegendSelect.id = route + '_names_select';
    divLegendSelect.className = "cov-btn-select"

    var divLegend = document.createElement('div');
    divLegend.id = route + '_legend';
    divLegend.className = "w3-panel w3-border w3-round-small";

    divLegendPermanent.appendChild(divLegendDateX);
    divLegendPermanent.appendChild(divLegendTotal);
    divLegendPanel.appendChild(divLegendSelected);
    divLegendPanel.appendChild(divLegendPopulation);
    divLegendPanel.appendChild(divLegendRatioHosp);

    divLegendContainer.appendChild(divLegendSelection);
    divLegendContainer.appendChild(divLegendSelect);
    divLegendContainer.appendChild(divLegend);
    // ======

    divLegendContainer.appendChild(divLegendPermanent);
    divLegendContainer.appendChild(divLegendPanel);
    divChartContainer.appendChild(divChart);
    divTable.appendChild(divChartContainer);
    divTable.appendChild(divLegendContainer);

    // div.appendChild(createPageButtonsBar(route));
    divPage.appendChild(titlePage)
    divPage.appendChild(createPageLoader(route));
    divPage.appendChild(divTable);

    mainDiv.appendChild(divPage);
    app.dataComplete[route] = {};
}

var createPageLoader = function (route) {
    var div = document.createElement('div')
    div.id = 'loader_' + route;
    div.className = "w3-container w3-center w3-animate-zoom loader-div";
    // div.title = "pour test";
    // div.innerHTML = 'profile ' + route;
    div.dataset.state = "";

    // var p = document.createElement('p');
    // p.innerHTML = 'Chargement des données...';

    var loader_div = document.createElement('div');
    loader_div.className = "loader";

    // div.appendChild(p);
    div.appendChild(loader_div);

    return div;
}
"use strict";

(function () {
    var app = {
        'routes': {
            'any': {
                'rendered': function () {
                    ;
                }
            },
            'region': {
                'rendered': function () {
                    document.getElementById('toolbars').style.display = "block"
                    document.getElementById('footer').classList.replace('footer-accueil', 'footer-chart');
                    getView(app.routeID)
                }
            },
            'dep': {
                'rendered': function () {
                    document.getElementById('toolbars').style.display = "block";
                    document.getElementById('footer').classList.replace('footer-accueil', 'footer-chart');
                    getView(app.routeID)
                }
            }
        },
        'default': 'accueil',
        'dataComplete': {},
        'postcodes': {},
        'parsexe': {
            dataFirstDay: "",
            sumDc: "",
            sumRad: ""
        },
        'indicateurs': {},
        'urgences': [],
        'testDepts': {},
        'state': "",
        'getUrl': function get(url) {
            return new Promise((resolve, reject) => {
                var req = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
                req.open('GET', url, true);
                req.onload = () => (req.status >= 200 && req.status < 400) ? resolve(req.response) : reject(Error(req.statusText));
                req.onerror = () => reject(req.status);
                req.send();
            });
        },
        'loadJS': function (url, onDone, onError) {
            if (!onDone) onDone = function () {};
            // if (!onState) onState = function() {};
            if (!onError) onError = function () {};

            // requête http
            var request = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
            request.open('GET', url, true);
            request.onload = onDone;
            // request.onreadystatechange = onState;
            request.send();
        },
        'routeChange': function () {
            app.routeID = location.hash.slice(1);
            app.route = app.routes[app.routeID];
            app.routeElem = document.getElementById(app.routeID);
            app.state = app.routeElem.dataset.state;
            app.routes['any'].rendered();
            document.getElementById('toolbars').style.display = "none";
            document.getElementById('footer').classList.replace('footer-chart', 'footer-accueil');
            if (app.route) {
                app.route.rendered();
            }
        },
        // The function to start the app.
        'init': function () {
            getPostCodes();

            var urlParSexe = 'https://www.data.gouv.fr/fr/datasets/r/63352e38-d353-4b54-bfd1-f1b3ee1cabd7';
            var urlUrgences = 'https://www.data.gouv.fr/fr/datasets/r/eceb9fb4-3ebc-4da3-828d-f5939712600a';
            var urlIndicateurs = 'https://www.data.gouv.fr/fr/datasets/r/f2d0f955-f9c4-43a8-b588-a03733a38921';
            var urlCasJour = "https://www.data.gouv.fr/fr/datasets/r/6fadff46-9efd-4c53-942a-54aca783c30c";

            // Capacité analytique de tests virologiques dans le cadre de l'épidémie COVID-19 SI-DEP
            var urlTestDepts = "https://www.data.gouv.fr/fr/datasets/r/0c230dc3-2d51-4f17-be97-aa9938564b39";
            // Taux d'incidence de l'épidémie de COVID-19 SI-DEP
            var urlCasPositifs = "https://www.data.gouv.fr/fr/datasets/r/19a91d64-3cd3-42fc-9943-d635491a4d76";
            // Données relatives aux résultats des tests virologiques COVID-19 SI-DEP
            var urlSIDEP = "https://www.data.gouv.fr/fr/datasets/r/406c6a23-e283-4300-9484-54e78c8ae675";

            getDataFiles(urlSIDEP, "SIDEP");

            getDataFiles(urlParSexe, "parsexe");
            getDataFiles(urlIndicateurs, "indicateurs");

            var routes = Object.keys(app.routes).slice(1);
            routes.forEach((route, i) => {
                createPage(route);
            });

            window.addEventListener('hashchange', function (e) {
                app.routeChange();
            });
            // if (!window.location.hash) {
            window.location.hash = "#accueil"; //app.default;
            // } else {
            //     app.routeChange();
            // }
        }
    };

    window.app = app;
})();

app.init();

function getPostCodes() {
    d3.csv('csv/insee-populations-estimation-2020-ensemble-departements.csv')
        .then(function (data) {
            data.forEach(item => {
                // console.log(item["Population municipale"].replace(/\s/gmi, ''));
                app.postcodes[item["Code département"]] = {
                    dep: item["Code département"],
                    dep_nom: item["Nom du département"],
                    region: item["Nom de la région"],
                    communes: item["Nombre de communes"],
                    population: item["Population municipale"].replace(/\s/gmi, '')

                }
            })
        });
}

function getDataFiles(url, type) {
    app.getUrl(url)
        .then((response) => {
            var delimiter = (/\,/gi).test(response) ? ',' :
                (/\;/gi).test(response) ? ";" : "\t";

            var dsv = d3.dsvFormat(delimiter);
            var data = dsv.parse(response);
            var fields = data.columns;

            if (type === "SIDEP") {
                getDataNestedSIDEP(data, fields);
            } else if (type === "indicateurs") {
                data.forEach(d => {
                    app.indicateurs[d[fields[1]]] = {
                        jour: d[fields[0]],
                        color: d[fields[4]]
                    }
                })
            } else if (type === "parsexe") {
                getDataNestedSexe(data, fields, function (dataFirstDay) {
                    getDataJour(dataFirstDay);
                });
            }
        })
        .catch((err) => {
            console.log(err);
        })
}

function getDataNestedSIDEP(data, fields, callback) {
    // filtrer pour tous âges = "0"
    data = data.filter(d => d[fields[4]] === "0");

    function cumulByArray(arr) {
        return arr.reduce((r, a) => {
            a += r[r.length - 1] || 0;
            r.push(a);
            return r;
        }, []);
    }

    var nestedData = d3.nest()
        .key(d => d[fields[0]])
        .rollup(v => {
            var cumulTestsPerDate = v.map((d, i) => {
                return {
                    date: d[fields[1]],
                    p: +d[fields[2]],
                    p_cumul: cumulByArray(v.map(d => +d[fields[2]]))[i],
                    t: +d[fields[3]],
                    t_cumul: cumulByArray(v.map(d => +d[fields[3]]))[i],
                    pop: ""
                }
            });
            // console.log(cumulTestsPerDate);
            return cumulTestsPerDate;
        })
        .entries(data);

    var dataTests = [];
    nestedData.forEach(d => {
        if (app.postcodes[d.key] !== undefined) {
            var dep = d.key;
            var region = app.postcodes[d.key].region;
            var pop = app.postcodes[dep]["population"];
            var obj = {};
            d.value.forEach(d => {
                obj = {
                    region: region,
                    dep: dep,
                    pop: +pop,
                    jour: d.date,
                    t: d.t,
                    testsCumul: d.t_cumul,
                    p: d.p,
                    casCumul: d.p_cumul,
                    cas_percent: d.p / d.t
                }
                dataTests.push(obj);
            })
        }
    });
    dataTests = dataTests.sort((a, b) => a["dep"] < b["dep"] ? -1 : a["dep"] > b["dep"] ? 1 : 0);
    app.dataComplete["dataTests"] = dataTests;
    document.getElementById("accueil").classList.replace("hide", "show");
    document.getElementById("footer").classList.replace("hide", "show");

    var labels = [...new Set(dataTests.map(d => d.region))]
        .sort((a, b) => a < b ? -1 : a > b ? 1 : 0);

    fillSelectSeries("", "accueil_regions_select", labels, "region");

    document.getElementById('accueil_title').textContent =
        'Données relatives aux résultats des tests virologiques COVID-19 (SI-DEP)';

    accueilChart(dataTests);
}

function getDataNestedSexe(data, fields, callback) {
    var dataToNest = [];
    var dataFiltered = data.filter(d => d["jour"] == "2020-03-18" && d["sexe"] == "0");

    dataFiltered.forEach(d => {
        var obj = {};
        if (app.postcodes[d[fields[0]]] !== undefined) {
            obj["region"] = app.postcodes[d[fields[0]]].region;
            obj["pop"] = +app.postcodes[d[fields[0]]].population;
            obj["dep"] = d[fields[0]];
            obj["jour"] = d[fields[2]]; // si DD/MM/YYYY : (d.jour).split("/").reverse().join("-");
            obj["hosp"] = +d[fields[3]].replace(/\s/gmi, '');
            obj["rea"] = +d[fields[4]].replace(/\s/gmi, '');
            obj["rad"] = +d[fields[5]].replace(/\s/gmi, '');
            obj["dc"] = +d[fields[6]].replace(/\s/gmi, '');
            dataToNest.push(obj);
        } else {
            // console.log(d["dep"], d.dc, d.hosp, d.rea, d.rad);
        }
    });

    app.parsexe.dataFirstDay = dataToNest;
    //.sort((a, b) => a["dep"] < b["dep"] ? -1 : a["dep"] > b["dep"] ? 1 : 0);

    var lastDay = d3.max([...new Set(data.map(d => d["jour"]))]);
    var dataLastDay = data.filter(d => d["jour"] === lastDay && d["sexe"] == "0");

    app.parsexe["sumDc"] = d3.sum(dataLastDay.map(d => +d.dc));
    app.parsexe["sumRad"] = d3.sum(dataLastDay.map(d => +d.rad));

    callback(dataToNest);
}

function getDataNestedJour(data, fields, dataFirstDay) {

    var dataToNest = [];
    data.forEach(d => {
        var obj = {};
        if (app.postcodes[d[fields[0]]] !== undefined) {
            obj["region"] = app.postcodes[d[fields[0]]].region;
            obj["pop"] = +app.postcodes[d[fields[0]]].population;
            obj["dep"] = d[fields[0]];
            obj["jour"] = d[fields[1]]; //(d.jour).split("/").reverse().join("-");
            obj["hosp"] = +d[fields[2]].replace(/\s/gmi, '');
            obj["rea"] = +d[fields[3]].replace(/\s/gmi, '');
            obj["dc"] = +d[fields[4]].replace(/\s/gmi, '');
            obj["rad"] = +d[fields[5]].replace(/\s/gmi, '');
            dataToNest.push(obj);
        } else {
            // console.log(d["dep"], d.dc, d.hosp, d.rea, d.rad);
        }
    })

    var dataComplete = dataFirstDay.concat(dataToNest)
        .sort((a, b) => a["dep"] < b["dep"] ? -1 : a["dep"] > b["dep"] ? 1 : 0);

    var allDates = [...new Set(dataComplete.map(d => d["jour"]))];
    var currentDate = allDates[allDates.length - 1];

    app.dataComplete["dataComplete"] = dataComplete;
    app.dataComplete["currentDate"] = currentDate;
    app.dataComplete["allDates"] = allDates;
    app.state = "loaded";

    getReady();
    return dataComplete;
}

function getDataJour(dataFirstDay) {
    var urlCasJour = "https://www.data.gouv.fr/fr/datasets/r/6fadff46-9efd-4c53-942a-54aca783c30c";

    app.getUrl(urlCasJour)
        .then((response) => {
            var regexDate = /\d{4}\-\d{2}\-\d{2}/gmi;

            var delimiter = (/\,/gi).test(response) ? ',' :
                (/\;/gi).test(response) ? ";" : "\t";

            var dsv = d3.dsvFormat(delimiter);
            var data = dsv.parse(response);

            var dataComplete = getDataNestedJour(data, data.columns, dataFirstDay);
        })
        .catch((err) => {
            console.log(err);
        });
}

function getView(route) {
    document.getElementById('loader_' + route).style.display = 'block';

    var unit = document.querySelector('input[name=units]:checked').value;
    var field = document.querySelector('input[name=fields]:checked').value;

    document.getElementsByName('fields').forEach(function (radio) {
        radio.onclick = function () {
            unit = document.querySelector('input[name=units]:checked').value;
            getDataChart(route, unit, radio.value);
        }
    });

    document.getElementsByName('units').forEach(function (radio) {
        radio.onclick = function () {
            field = document.querySelector('input[name=fields]:checked').value;
            getDataChart(route, radio.value, field);
        }
    });

    getDataChart(route, unit, field);
}

function getReady() {
    document.getElementById('accueil').classList.replace('hide', 'show');
    setTimeout(() => {
        if (app.state) {
            document.getElementById("loader_accueil").innerHTML = "";
            document.querySelectorAll('.pointerNone').forEach((el) => {
                el.classList.replace('pointerNone', 'pointerAuto');
            });
        }
    }, 100);
}
"use strict";

function getDataChart(route, unit, field) {

    var dataComplete = app.dataComplete["dataComplete"];

    var currentDate = app.dataComplete["currentDate"];
    var dates = app.dataComplete["allDates"];
    var dateRange = d3.extent(app.dataComplete.allDates);

    var names = [...new Set(dataComplete.map(d => d[route]))];

    // fillSelectSeries('select_dates', dates, 'dates');
    fillSelectSeries(route, '_names_select', names, "");


    var nestedData = function (dataSeries) {
        return d3.nest()
            .key(d => d["jour"])
            .key(d => d[route])
            .rollup(v => {
                var cumul = 0;
                var total = d3.sum(v.map(d => d[field]));
                var hosp = d3.sum(v.map(d => d.hosp));
                var pop = d3.sum(v.map(d => d.pop))

                cumul += hosp;
                return {
                    total: total,
                    hosp: hosp,
                    // hom: hom,
                    // fem: fem,
                    pop: pop,
                    cumul: cumul
                }
            })
            .entries(dataSeries)
    };

    var selectedUnits = {
        'nombre': "en nombre",
        'proportions': "en proportion"
    }

    var selectedFields = {
        'hosp': "Personnes nouvellement hospitalisées",
        'rea': "Personnes nouvellement en réanimation ou en soins intensifs",
        'rad': "Personnes nouvellement retournées à domicile",
        'dc': "Personnes nouvellement décédées à l'hôpital",
        'dc_ehpad': "Personnes nouvellement décédées en ehpad",
        'dc_ehpad': "Personnes nouvellement décédées (hôpital + ehpad)",
    }

    var zone = route === "region" ? "région" : route === "dep" ? "département" : "total";
    var titleGraph = selectedFields[field] + ' par ' + zone; // + ' (' + selectedUnits[unit] + ')';
    document.getElementById(route + '_title').textContent = titleGraph;


    var dataset = [];
    var population = {};
    var ratioHosp = {};
    var cumulTotalSeries = {};
    var seriesCumul = {};

    nestedData(dataComplete).forEach((d, i) => {
        var date = d.key;
        var values1 = d.values;
        var row = [];
        var rowHosp = {};
        var total, hosp, hom, fem, pop,
            percent, percentHosp;

        var cumul = 0;
        values1.forEach((d, j) => {
            var name = d.key;
            total = d.value.total;
            hosp = d.value.hosp;
            pop = d3.sum(d.value.pop);

            percent = !isNaN(total / pop) && (total / pop) !== Infinity ? total / pop : 0;
            percentHosp = !isNaN(total / hosp) && (total / hosp) !== Infinity ? total / hosp : 0;

            (unit === 'nombre') ? row.push(total): row.push(percent);

            seriesCumul[name] = total;
            rowHosp[name] = percentHosp;
            population[name] = pop;
            cumul += total;
        });
        ratioHosp[date] = rowHosp;
        row.unshift(new Date(date));
        dataset.push(row);
        cumulTotalSeries[date] = cumul;

    });

    names.unshift("jour");

    var config = {
        names: names,
        dataset: dataset,
        cumulTotalSeries: cumulTotalSeries,
        ratioHosp: ratioHosp,
        dateRange: dateRange,
        population: population,
        seriesCumulSorted: Object.entries(seriesCumul).sort((a, b) => b[1] - a[1]),
        selectedUnits: selectedUnits,
        selectedFields: selectedFields,
        populationTotale: d3.sum(Object.entries(population).map(d => d[1]))

    };

    var cumulSerie = d3.sum(config.seriesCumulSorted.map(d => d[1]));
    var cumulSeriePercent = cumulSerie / config.populationTotale;

    document.getElementById(route + '_legend_dateX').innerHTML = '<span style="font-size: 1.5em; font-weight: 600">' +
        moment(currentDate, "YYYY-MM-DD").format("dddd, DD MMMM YYYY") + '</span>';

    var cumul = unit === "nombre" ?
        'Total: ' + localeFR.format(",")(cumulSerie) + ' (France entière)' :
        'réprésente ' + localeFR.format(".2%")(cumulSeriePercent) + ' de la population (France entière)';

    document.getElementById(route + '_legend_total').innerHTML = cumul;

    document.getElementById(route + '_divLegendPermanent').style.display = "block";

    launchChart(route, config, unit, field, currentDate);
}


function launchChart(route, config, unit, field, currentDate) {
    var field = document.querySelector('input[name=fields]:checked').value;
    var scale = document.querySelector('input[name = "scales"]:checked').value;

    var names = config.names.slice(1, config.names.length - 1);
    names = names.sort((a, b) => a < b ? -1 : a > b ? 1 : 0);

    var setDateSelection = function (index) {
        var dates = app.dataComplete["allDates"];

        var date = (index !== undefined) ?
            moment(dates[index], "YYYY-MM-DD").format("dddd, DD MMMM YYYY") :
            moment(config.dateRange[1], "YYYY-MM-DD").format("dddd, DD MMMM YYYY");
        return date;
    };

    var resetNameSelection = function (index) {
        var select = document.getElementById(route + '_names_select');
        select.selectedIndex = 0;
    };

    var getZone = (dep) => {
        return (app.indicateurs[dep].color == "rouge") ?
            '<i class="gg-shape-circle-red"></i>' :
            (app.indicateurs[dep].color == "orange") ?
            '<i class="gg-shape-circle-orange"></i>' :
            '<i class="gg-shape-circle-green"></i>';
    }

    var setLabeledDataLegend = (label, y) => {
        return route === 'region' ? '<b>' + label +
            ' </b><span class="w3-round w3-blue-grey" style="padding: 2px 4px">' + y + '</span>' :
            '(<b>' + label + '):</b><span class="w3-round w3-blue-grey" style="padding: 2px 4px">' + y + '</span>';
    }

    var setLabeledData = (label, y) => {
        return '(<b>' + getZone(label) + ' ' + label + '):</b> ' + y;

    }

    document.getElementById(route + '_dates_container').textContent = setDateSelection();

    var legendFormatter = function (data) {
        var html = "";
        if (data.x == null) {
            html += route === "region" ?
                config.seriesCumulSorted
                .map(arr => '<b>' + arr[0] + ':</b> ' + arr[1]).join('<br>') + '</div>' :
                config.seriesCumulSorted
                .map(arr => setLabeledData(arr[0], arr[1])).join(' ') + '</div>';

            return html;
        }
        var date_YYYYMMDD = moment(data.xHTML, "YYYY/MM/DD HH:mm").format("YYYY-MM-DD");

        document.getElementById(route + '_legend_dateX').innerHTML =
            '<span style="font-size: 1.5em; font-weight: 600">' +
            moment(data.xHTML, "YYYY/MM/DD HH:mm").format("dddd, DD MMMM YYYY") + '</span>';

        var cumulPercent = config.cumulTotalSeries[date_YYYYMMDD] / config.populationTotale;

        var cumul = unit === "nombre" ?
            'France entière : <span class="w3-round w3-blue-grey" style="padding: 2px 5px">' + localeFR.format(",")
            (d3.sum(data.series.map(a => a.y))) + '</span>' :
            'France entière : <span class="w3-round w3-blue-grey" style="padding: 2px 5px">' + localeFR.format(
                ".2%")(cumulPercent);

        document.getElementById(route + '_legend_total').innerHTML = cumul;

        data.series.sort((a, b) => b.y - a.y);

        data.series.forEach(function (serie, i) {
            if (!serie.isVisible) return;

            var population = config.population[serie.labelHTML];

            var ratioHosp = config.ratioHosp[date_YYYYMMDD][serie.labelHTML];

            var labeledData = route !== "region" ?
                setLabeledData(serie.labelHTML, serie.yHTML) :
                serie.labelHTML + ': ' + serie.yHTML;

            var serieColor = serie.color;

            if (serie.isHighlighted) {
                if (route !== "region") {
                    document.getElementById(route + '_legend_selected').style.background =
                        serie.color.replace(/rgb/, 'rgba').replace(/\)/, ', 0.2)');

                    document.getElementById(route + '_legend_selected').innerHTML =
                        '<span style="font-size: 1.5em; font-weight: 600">' + '(' + getZone(serie.labelHTML) +
                        ' ' + serie.labelHTML + ') ' +
                        app.postcodes[serie.labelHTML].dep_nom + ': ' +
                        '<span class="w3-round w3-blue-grey" style="padding: 2px 4px">' + serie.yHTML +
                        '</span></span>';

                    document.getElementById(route + '_legend_population').innerHTML =
                        '<span>sur une population de ' + localeFR.format(",")(population) + '</span>';

                    if (field === "rea") {
                        var ratioHospHTML = ratioHosp <= 1 ? localeFR.format(".2%")(ratioHosp) :
                            '<span style="color:red">' + localeFR.format(".2%")(ratioHosp) + ' ??</span>';
                        document.getElementById(route + '_legend_ratioHosp').innerHTML =
                            '<span>' + ratioHospHTML + ' des personnes hospitalisées</span>';
                    } else {
                        document.getElementById(route + '_legend_ratioHosp').innerHTML = "";
                    }
                } else {
                    labeledData = '<b>' + labeledData + '</b>';

                    document.getElementById(route + '_legend_selected').style.background =
                        serie.color.replace(/rgb/, 'rgba').replace(/\)/, ', 0.2)');

                    document.getElementById(route + '_legend_selected').innerHTML =
                        '<span style="font-size: 1.5em; font-weight: 600">' + setLabeledDataLegend(serie.labelHTML,
                            serie.yHTML) + '</span>';

                    document.getElementById(route + '_legend_population').innerHTML =
                        '<span>sur une population de ' + localeFR.format(",")(population) + '</span>';

                    if (field === "rea") {
                        var ratioHospHTML = ratioHosp <= 1 ? localeFR.format(".2%")(ratioHosp) :
                            '<span style="color:red">' + localeFR.format(".2%")(ratioHosp) + ' ??</span>';
                        document.getElementById(route + '_legend_ratioHosp').innerHTML =
                            '<span>' + ratioHospHTML + ' des personnes hospitalisées</span>';
                    } else {
                        document.getElementById(route + '_legend_ratioHosp').innerHTML = "";
                    }
                }
            } else {
                labeledData = '<b style="opacity: 0.3">' + labeledData + '</b>';
            }
            // html += (route === "region") ?
            //     ' ' + serie.dashHTML + ' ' + labeledData + '<br>' :
            //     ' ' + serie.dashHTML + ' ' + labeledData;
            html += (route === "region") ? labeledData + '<br>' : labeledData;
        });
        html += '</div>'
        return html;
    }

    var g2 = new Dygraph(
        document.getElementById(route + '_chart'),
        config.dataset, {
            labels: config.names,
            pop: config.population,
            height: Math.round(window.innerHeight * 0.75),
            highlightCircleSize: 1,
            drawPoints: true,
            plotter: barChartPlotter,
            highlightSeriesOpts: {
                strokeWidth: 3,
                strokePattern: [20, 20],
                highlightCircleSize: 6
            },
            highlightSeriesBackgroundAlpha: 0.2,
            // title: '<span class="title-graph" style="font-size: 16px; font-weight: normal">(bandes grisées = WE)</span>',
            legend: 'always',
            labelsDiv: document.getElementById(route + '_legend'),
            legendFormatter: legendFormatter,
            yAxisLabelWidth: 200,
            ylabel: config.selectedUnits[unit],
            customBars: false,
            showRangeSelector: true,
            rangeSelectorPlotFillColor: 'white',
            rangeSelectorAlpha: route === "region" ? 0.5 : 0.1,
            rangeSelectorPlotFillGradientColor: field === 'hosp' ? "orangered" : field === 'rad' ? 'green' : field ===
                'dc' ? 'black' : 'red',
            // showRoller: true,
            logscale: scale === 'log' ? true : false,
            underlayCallback: hightlight_WE,
            axes: {
                x: {
                    axisLabelFormatter: function (d, gran, opts) {
                        return moment(d).format("DD MMM"); // A REVOIR
                    },
                },
                y: {
                    valueFormatter: function (y) {
                        return unit !== 'nombre' ? localeFR.format(".2%")(y) : formatThousands(y);
                    },
                    axisLabelFormatter: function (y) {
                        return unit !== 'nombre' ? localeFR.format(".2%")(y) : formatThousands(y);
                    },
                    pixelsPerLabel: 50,
                    axisLabelWidth: 80
                }
            },
            unhighlightCallback: function (e, x, pts, row) {
                g2.clearSelection();
                legendPermanentHideShow(false);
                legendPanelDivHideShow(false);

                document.getElementById(route + '_dates_container').textContent = setDateSelection();
                resetNameSelection();
            },
            highlightCallback: function (e, x, pts, row) {
                legendPermanentHideShow(true);
                legendPanelDivHideShow(true);

                document.getElementById(route + '_dates_container')
                    .innerHTML = '<span style="color: #607d8b">' + setDateSelection() + '</span>';
                resetNameSelection();
            }
        }
    );

    document.getElementById('loader_' + route).style.display = 'none';

    document.getElementsByName('scales').forEach(function (radio) {
        radio.onclick = function () {
            var val = radio.value === 'log' ? true : false;
            g2.updateOptions({
                logscale: val
            });
        }
    });

    var changeDateSelection = function (side) {
        var name = document.getElementById(route + '_names_select').value;

        if (name == "") {
            g2.clearSelection();
            legendPanelDivHideShow(false);
        }

        legendPermanentHideShow(true);

        var dates_container = document.getElementById(route + '_dates_container');
        var dates = app.dataComplete.allDates;
        var date = dates_container.textContent.replace(/au /, '');

        var rank = dates.map(d => moment(d, "YYYY-MM-DD").format("dddd, DD MMMM YYYY")).indexOf(date);
        var index = side === 'left' ? rank - 1 : rank + 1;

        var className = side === 'left' ? '<span class="w3-animate-right">' : '<span class="w3-animate-lrft">';

        if (index >= 0 && index < app.dataComplete.allDates.length) {
            var newDate = moment(app.dataComplete.allDates[index], "YYYY-MM-DD").format("dddd, DD MMMM YYYY");
            dates_container.innerHTML = '<span class="w3-animate-right">' + newDate + '</span>';

            g2.setSelection(index, name);

            g2.setSelection(g2.getSelection(), g2.getHighlightSeries(), true)
        }
    }

    document.getElementById(route + '_names_select').onchange = function () {
        var name = this.value;

        if (name == "") {
            g2.clearSelection();
            legendPanelDivHideShow(false);
            return;
        }
        legendPermanentHideShow(true);
        legendPanelDivHideShow(true);

        var dates_container = document.getElementById(route + '_dates_container');
        var dates = app.dataComplete.allDates;
        var date = dates_container.textContent.replace(/au /, '');
        var indexDate = dates.map(d => moment(d, "YYYY-MM-DD").format("dddd, DD MMMM YYYY")).indexOf(date);


        g2.setSelection(indexDate, name);

        g2.setSelection(g2.getSelection(), g2.getHighlightSeries(), true)

    }

    document.getElementById(route + '_left_dates_btn').onclick = function (e) {
        changeDateSelection('left');
    };
    document.getElementById(route + '_right_dates_btn').onclick = function (e) {
        changeDateSelection('right');
    };


    var legendPermanentHideShow = function (flag) {
        if (flag) {
            document.getElementById(route + '_divLegendPermanent').style.display = "block";
        } else {
            document.getElementById(route + '_divLegendPermanent').style.display = "none";
        }

    }

    var legendPanelDivHideShow = function (flag) {
        if (flag) {
            document.getElementById(route + '_divLegendPanel').style.display = "block";
        } else {
            document.getElementById(route + '_divLegendPanel').style.display = "none";
        }

    }

    function getChartSelection() {
        var sel = g2.getSelection();
        for (var i = 0; i < sel.length; i++) {
            // console.log(sel[i])
        }
    }
}

function fillSelectSeries(route, divId, options, type) {
    divId = route ? route + divId : divId;

    var select = document.getElementById(divId);
    while (select.options.length)
        select.options.remove(0);

    type = type ? type : route;
    var optionChoisir = type === "dep" ? "choisir un département..." : "choisir une région...";

    if (options && options.length) {
        var opt = document.createElement("option");
        opt.value = "";
        opt.text = optionChoisir;
        select.add(opt, select.options[0]);
        var index = options.length - 1;
        // // voir pour ajouter un paramètre "extra" pour un select sur options dates par exemple
        // options = type === 'dates' ? options.reverse() : options;

        for (var i = 0; i < options.length; i++) {
            var option = document.createElement("option");
            var optionText = type === "dep" ? '(' + options[i] + ') ' +
                app.postcodes[options[i]].dep_nom : options[i];

            option.text = optionText;
            option.value = type === 'dates' ? i : options[i];
            select.add(option, select.options[1 + i]);
            // index--;
        }
    }
}
"use strict";

function setInfos() {
    document.getElementById('data_infos').innerHTML = `<p style="text-align: justify;">En page d'accueil, données relatives aux résultats des tests virologiques COVID-19 SI-DEP, en déploiement depuis le 13 mai 2020. Visualisation en proportion de population testée (pourcentage établi à partir du nombre cumulé de tests journaliers réalisés). Sur France entière, région entière et département.
        Description complète sur <a href="https://www.data.gouv.fr/fr/datasets/donnees-relatives-aux-resultats-des-tests-virologiques-covid-19/">data.gouv.fr</a><br>
        Pour les onglets "Régions" et "Départements" : le nombre quotidien de personnes nouvellement hospitalisées, en réanimation, décédées ou retours à domicile.<br>
        Pour les départements des indicateurs couleurs d'activité épidémique COVID-19 (zone rouge, orange ou verte).<br>
        Sur tous les graphiques, les bandes grisées pour marquer les samedis & dimanches.<br>
        Les jeux de données sont accessibles sur data.gouv.fr et mis à jour quotidiennement aux alentours de 19h00 par <a href="https://www.data.gouv.fr/fr/organizations/sante-publique-france/" title="Sante Publique France" target="_blank">Sante Publique France</a>.<br>
        Ce prototyme d'application n'est prévu qu'à fin de tests.
        Merci de signaler toutes erreurs, omissions, bugs ou incohérences dans la répresentation des données.<br>
        Par ailleurs, certains fichiers pouvant comporter des anomalies du fait des difficultés de collecte des données.
        Description complète et rapport d'erreurs sur le site <a href="https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/" target="_blank" title="data.gouv.fr"> data.gouv.fr</a>
        </p>`
}

var getDepNom = (selected, label) => (selected === "dep" && label !== "région entière") ?
    '<span>(' + label + ') ' + app.postcodes[label].dep_nom + '</span>' :
    '<span>' + label + '</span>';

function valuesDashboard(date, selected, label, dict, dates, type) {
    var last7days = Object.entries(dict)
        .sort((a, b) => a[0] - b[0])
        .slice(-7)
        .map(d => Object.entries(d[1]).filter(d => d[0] == label));

    // cumul 7 jours glissants
    var last7days_t = d3.sum(last7days.map(d => d3.sum(d.map(d => d[1].t))));
    var last7days_p = d3.sum(last7days.map(d => d3.sum(d.map(d => d[1].p))));

    var last7days_mean = last7days_p / last7days_t;

    var last7days_pop = last7days[0][0][1].pop;
    var taux_incidence = localeFR.format(".2f")((last7days_p * 100000) / last7days_pop);

    var values = dict[date][label];
    var mean = values.t_nombre / (dates.length / 7);

    // var tableHeaders = selected !== "dep" ? ["région", "taux de positivité <sup>(4)</sup>", "taux de positivité <sup>(6)</sup>"] : ["département", "taux de positivité <sup>(4)</sup>", "taux de positivité <sup>(6)</sup>"];

    // var sortedCasPositifs = Object.entries(dict[date]).sort((a, b) => b[1].cas_percent - a[1].cas_percent).map(d => [getDepNom(selected, d[0]), localeFR.format(".2%")(d[1].cas_percent), localeFR.format(".2%")(d[1].p_nombre / d[1].pop)]);

    // var tableCasPositifs = createTable(tableHeaders, sortedCasPositifs);

    var getLabelMinMax = (label) => selected === "dep" && app.postcodes[label] !== undefined ? '(' + label + ') ' + app.postcodes[label].dep_nom : label;

    var getValuesMinMax = (prop, val) => {
        var value = val === "min" ? Infinity : -Infinity,
            label,
            pop,
            flag,
            key;
        for (var key in dict[date]) {
            if (key !== "France entière" && key !== "région entière") {
                flag = val === "min" ? dict[date][key][prop] < value : dict[date][key][prop] > value;
                if (flag) {
                    value = dict[date][key][prop];
                    label = getLabelMinMax(key);
                    pop = dict[date][key].pop;
                }
            }
        }
        return {
            value: value,
            label: label,
            pop: pop
        }
    }

    var dataDash = '<p><b>Au ' + moment(date, "YYYY-MM-DD").format("LL") +
        ' (' + getDepNom(selected, label) + ')' + '</b></p><ul class="w3-ul list-current">';

    var dataDash = '<p><b>Au ' + moment(date, "YYYY-MM-DD").format("LL") +
        ' (' + getDepNom(selected, label) + ')' + '</b></p><ul class="w3-ul list-current">';

    dataDash += type !== "indic" ? '<li>Tests effectués (ce jour) : <b>' + convertToMillions(values.t) + '</b></li>' : '';
    dataDash += type === "dash" ? '<li>Tests effectués (7 jours glissants) : <b>' + convertToMillions(Math.round(last7days_t)) + '</b></li>' : '';
    dataDash += type !== "indic" ? '<li>Tests effectués (par semaine) : <b>' + convertToMillions(Math.round(mean)) + '</b></li>' : '';
    dataDash += type !== "indic" ? '<li>Tests effectués (cumul) : <b>' + convertToMillions(values.t_nombre) + ' </b><sup>(1)</sup></li>' : '';

    dataDash += type !== "indic" ? '<br><li>Cas positifs (ce jour) : <b>' + convertToMillions(values.p) + '</b> </li>' : '';
    dataDash += type !== "indic" ? '<li>Cas positifs (cumul) : <b>' + convertToMillions(values.p_nombre) + ' </b></li>' : '';
    dataDash += type !== "indic" ? '<li>Taux de positivité (ce jour) : <b>' + localeFR.format(".2%")(values.p / values.t) + '</b> <sup>(4)</sup></li>' : '';

    dataDash += type === "dash" ? '<li>Taux de positivité (7 jours glissants) : <b>' + localeFR.format(".2%")(last7days_mean) + '</b> <sup>(4)</sup></li>' : '';

    dataDash += type === "dash" ?
        '<br><li>Taux de positivité le plus élevé (ce jour) pour ' + getValuesMinMax("cas_percent", "max").label +
        ' : <b>' + localeFR.format(".2%")(getValuesMinMax("cas_percent", "max").value) + '</b></li>' : '';

    dataDash += type !== "indic" ?
        '<br><li>Taux d\'incidence pour ' + label + ' : <b>' + taux_incidence + '</b> <sup>(6)</sup></li>' : '';

    dataDash += type === "dash" ? '<br><li><b>' + localeFR.format(".2%")(values.t_nombre / values.pop) + '</b> <sup>(2)</sup> de population testée soit <b>' + convertToMillions(values.t_nombre) + '</b> personnes sur <b>' + convertToMillions(values.pop) + '</b> <sup>(3)</sup> ' + label + '</li>' :
        type === "current" ?
        '<br><li><b>' + localeFR.format(".2%")(values.t_nombre / values.pop) + '</b> <sup>(2)</sup> de population testée soit <b>' + convertToMillions(values.t_nombre) + '</b> personnes sur <b>' + convertToMillions(values.pop) + '</b> <sup>(3)</sup> ' + label + '</li>' : '';

    // dataDash += type === "indic" ? tableCasPositifs : '';

    dataDash += '</ul>';

    return dataDash;
}

function getDepts(data) {
    var depts = {};

    var nestedData = d3.nest()
        .key(d => d.region)
        .rollup(v => {
            return {
                depts: [...new Set(v.map(d => d.dep))]
            }
        })
        .entries(data);

    nestedData.forEach(d => {
        depts[d.key] = d.value.depts
    })
    return depts;
}

function notesExpand(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}

document.getElementById("accueil_regions_select").addEventListener('change', function (e) {
    var data = app.dataComplete["dataTests"];

    document.getElementById('accueil_legend_current').innerHTML = "survolez ou touchez pour sélectionner";
    var data = (this.value === "") ? data : data.filter(d => d.region === this.value);
    var selected = (this.value === "") ? undefined : "dep";

    accueilChart(data, selected);
}, false);

function accueilChart(data, selected) {
    setInfos();

    selected = (selected !== undefined) ? selected : "region";
    var labelMean = selected == "dep" ? "région entière" : "France entière";

    var dates = [...new Set(data.map(d => d["jour"]))];
    var lastDate = dates[dates.length - 1];

    var unit;
    var field = document.querySelector('input[name=fields]:checked').value;
    var scale = document.querySelector('input[name = "scales"]:checked').value;

    var nestedData = d3.nest()
        .key(d => d.jour)
        .key(d => d[selected])
        .rollup(v => {
            return {
                pop: d3.sum(v.map(d => d.pop)),
                t_cumul: d3.sum(v.map(d => d.testsCumul)),
                t_percent: d3.sum(v.map(d => d.testsCumul)) / d3.sum(v.map(d => d.pop)),
                p_cumul: d3.sum(v.map(d => d.casCumul)),
                p_percent: d3.sum(v.map(d => d.casCumul)) / d3.sum(v.map(d => d.testsCumul)),
                t: d3.sum(v.map(d => d.t)),
                p: d3.sum(v.map(d => d.p)),
                cas_percent: d3.mean(v.map(d => d.cas_percent))
            }
        })
        .entries(data)

    var dataset = [];
    var seriesValues = {};
    var namesByPercent = {};
    var propPopFrance = {};
    var testsCumulTotal = 0;

    var getTnombreByDay = function (obj, key) {
        var total = 0;
        for (var item in obj) {
            total += obj[item][key]
        }
        return total;
    }

    var getLast7days = (dict) => dict.map(d => d.values);

    nestedData.forEach((d, index) => {
        var length = dates.length - 1;
        var end = length - index;
        var start = (index <= 6) ? 0 : index - 6;

        var date = d.key;
        var values1 = d.values;
        var population = 0;
        var t_cumul = 0;
        var p_cumul = 0;
        var row = [];
        var obj = {};

        values1.sort(function (a, b) {
            return b.value.t_percent - a.value.t_percent;
        });

        values1.forEach((d, j) => {
            var name = d.key;
            obj[name] = {
                t_nombre: d.value.t_cumul,
                t_percent: d.value.t_percent,
                pop: d.value.pop,
                p_nombre: d.value.p_cumul,
                p_percent: d.value.p_percent,
                t: d.value.t,
                p: d.value.p,
                cas_percent: d.value.cas_percent
            };
            namesByPercent[name] = d.value.t_percent;

            row.push(d.value.t_percent)

            t_cumul += d.value.t_cumul;
            p_cumul += d.value.p_cumul;
            population += d.value.pop;
            testsCumulTotal += t_cumul;
        });

        obj[labelMean] = {
            t_nombre: t_cumul,
            t_percent: (t_cumul / population),
            pop: population,
            p_nombre: p_cumul,
            p_percent: (p_cumul / population),
            t: getTnombreByDay(obj, "t"),
            p: getTnombreByDay(obj, "p"),
            cas_percent: getTnombreByDay(obj, "p") / getTnombreByDay(obj, "t")
        };

        namesByPercent[labelMean] = (t_cumul / population);

        row.push(t_cumul / population);

        propPopFrance[date] = (t_cumul / population);

        seriesValues[date] = obj;

        row.sort((a, b) => b - a);

        row.unshift(new Date(date));
        dataset.push(row);
    });

    document.getElementById('accueil_dash').innerHTML = valuesDashboard(lastDate, selected, labelMean, seriesValues, dates, "dash");

    document.getElementById('indicateurs_dash').innerHTML = valuesDashboard(lastDate, selected, labelMean, seriesValues, dates, "indic");

    var labelsSorted = Object.entries(namesByPercent).sort((a, b) => b[1] - a[1]);
    var labels = labelsSorted.map(d => d[0]);

    var last7daysByLabel = {};
    labels.forEach(label => {
        last7daysByLabel[label] = { tests: 0, positifs: 0, cas_percent: 0, pop: 0};
    })

    var getLast7days = (dict) => dict.map((d, i) => {
        for (var key in d[1]) {
            last7daysByLabel[key]["t_percent"] = d[1][key].t_percent;
            last7daysByLabel[key]["tests"] += d[1][key].t;
            last7daysByLabel[key]["positifs"] += d[1][key].p;
            last7daysByLabel[key]["cas_percent"] = d[1][key].cas_percent;
            last7daysByLabel[key]["pop"] = d[1][key].pop;
        };
    });

    getLast7days((Object.entries(seriesValues)).slice(-7));

    var tableHeaders = selected !== "dep" ? ["région", "tests <sup>(2)</sup>", "positivité <sup>(5)</sup>", "incidence<sup>(6)</sup>", "ratio <sup>(7)</sup>"] : ["département", " tests <sup>(2)</sup>", "positivité <sup>(5)</sup>", "incidence <sup>(6)</sup>", "ratio <sup>(7)</sup>"];


    var sortedCasPositifs = Object.entries(last7daysByLabel).sort((a, b) => ((b[1].positifs * 100000) / b[1].pop)/(b[1].t_percent*100) - ((a[1].positifs * 100000) / a[1].pop)/(a[1].t_percent*100))
        .map(d => [
            getDepNom(selected, d[0]),
            localeFR.format(".2%")(d[1].t_percent),
            localeFR.format(".2%")(d[1].positifs / d[1].tests),
            localeFR.format(".2f")((d[1].positifs * 100000) / d[1].pop),
            localeFR.format(".2f")(((d[1].positifs * 100000) / d[1].pop)/(d[1].t_percent*100)),
        ]);

    var tableCasPositifs = createTable(tableHeaders, sortedCasPositifs);

    document.getElementById('indicateurs_dash').innerHTML = '<p><b>Au ' + moment(lastDate, "YYYY-MM-DD").format("LL") +
        '</b> (pour trier, cliquez sur un titre de colonne)</p>' + tableCasPositifs;

    document.getElementById('accueil_dash').innerHTML = valuesDashboard(lastDate, selected, labelMean, seriesValues, dates, "dash");

    var series = {};
    labels.forEach((label, i) => {
        if (label !== labelMean) {
            series[label] = {
                plotter: barChartPlotter,
            }
        } else {
            series[label] = {
                strokeWidth: 3,
                stepPlot: true,
                fillGraph: true,
                color: "#550000"
            }
        }
    });

    var legendFormatter = function (data) {
        var getTotalSelection = function (percent, nombre) {
            var selected_region = document.getElementById("accueil_regions_select").value;
            return "";
            // selected_region !== "" ?
            //     ' => région entière ' + formatThousands(nombre) +
            //     ' tests (' + localeFR.format(".2%")(percent) + ')' :
            //     ' => France entière ' + formatThousands(nombre) +
            //     ' tests (' + localeFR.format(".2%")(percent) + ')';
        }

        var lineDiv = (color, label) => {
            return label == labelMean ?
                '<div class="dygraph-legend-line" style="border-bottom-width: 5px;"></div>' :
                '<div class="dygraph-legend-line" style="border-top: solid 2px ' + color + '; border-bottom: solid 10px ' + darkenColor(color, 0.3) + '; vertical-align: bottom;"></div>';
        };
        if (data.x == null) {

            return '<b>' + this.getLabels()[0] + ' ' +
                moment(lastDate, "YYYY-MM-DD").format("LL") + ' </b>' + getTotalSelection(seriesValues[lastDate][labelMean].t_percent, seriesValues[lastDate][labelMean].t_nombre) + '<br>' + data.series
                .map(series => lineDiv(series.color, series.labelHTML) + ' ' + getDepNom(selected, series.labelHTML) + ' : ' + '<span class="w3-round" style="padding: 2px 5px">' +
                    convertToMillions(seriesValues[lastDate][series.labelHTML].t_nombre) + ' tests <sup>(1)</sup>, ' +
                    localeFR.format(".2%")(namesByPercent[series.labelHTML]) + ' <sup>(2)</sup></span>')
                .join('<br>');
        }
        var date = moment(data.xHTML, "YYYY-MM-DD").format("YYYY-MM-DD");
        var html = '<b>' + this.getLabels()[0] + ' ' + moment(data.xHTML, "YYYY-MM-DD").format("LL") + '</b>' + getTotalSelection(seriesValues[lastDate][labelMean].t_percent, seriesValues[lastDate][labelMean].t_nombre);

        data.series.forEach(function (series) {
            if (!series.isVisible) return;
            var values = seriesValues[date][series.label];
            var mean = Math.round(values.t_nombre / (dates.length / 7));

            var labeledData = getDepNom(selected, series.label) + ' : <span class="w3-round" style="padding: 2px 5px;">' + convertToMillions(values.t_nombre) + ' tests <sup>(1)</sup>, </span> ' + series.yHTML + ' <sup>(2)</sup>';

            var labeledDataHighlighted = getDepNom(selected, series.label) + ' : ' +
                convertToMillions(values.t_nombre) + ' tests <sup>(1)</sup>, <span class="w3-round" style="padding: 2px 5px; background-color:' + series.color + '; color:white">' + series.yHTML + '</span> <sup>(2)</sup>';

            var labeledDataCurrent = valuesDashboard(date, selected, series.label, seriesValues, dates, "current")

            if (series.isHighlighted) {
                labeledData = '<b>' + labeledDataHighlighted + '</b>';
                document.getElementById('accueil_legend_current').innerHTML = labeledDataCurrent;
            }
            html += '<br>' + lineDiv(series.color, series.labelHTML) + ' ' + labeledData;
        });
        return html;
    }
    // ajout colonne ("jour") pour X axis
    labels.unshift("Au");

    var g3 = new Dygraph(
        document.getElementById("accueil_chart"),
        dataset, {
            labels: labels,
            height: Math.round(window.innerHeight * 0.80),
            highlightCircleSize: 2,
            highlightSeriesOpts: {
                strokeWidth: 2,
                strokePattern: [20, 20],
                highlightCircleSize: 6
            },
            highlightSeriesBackgroundAlpha: 0.5,
            title: '<span class="title-graph" style="font-size: 16px; font-weight: normal">(bandes grisées = weekend)</span>',
            labelsDiv: document.getElementById('accueil_legend'),
            legend: 'always',
            legendFormatter: legendFormatter,
            unhighlightCallback: function (e, x, pts, row) {
                g3.clearSelection();
            },
            underlayCallback: hightlight_WE,
            showRangeSelector: true,
            rangeSelectorPlotFillColor: 'white',
            rangeSelectorAlpha: 0.5,
            rangeSelectorPlotFillGradientColor: "#550000",
            yAxisLabelWidth: 200,
            ylabel: "tests en pourcentage par population (données brutes)",
            axes: {
                x: {
                    axisLabelFormatter: function (d, gran, opts) {
                        return moment(d).format("DD MMM"); // A REVOIR
                    },
                },
                y: {
                    valueFormatter: function (y) {
                        return unit !== 'nombre' ? localeFR.format(".2%")(y) : formatThousands(y);
                    },
                    axisLabelFormatter: function (y) {
                        return unit !== 'nombre' ? localeFR.format(".2%")(y) : formatThousands(y);
                    },
                    pixelsPerLabel: 50,
                    axisLabelWidth: 80
                }
            },
            series: series
        });
}

function openDash(tabName) {
    if (tabName == "description") {
        document.getElementById('dateFrom_title').style.display = "none";
        document.getElementById('accueil_regions_select').style.display = "none";
    } else {
        document.getElementById('dateFrom_title').style.display = "block";
        document.getElementById('accueil_regions_select').style.display = "block";
    }
    var i;
    var x = document.getElementsByClassName("dash");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    document.getElementById(tabName).style.display = "block";
}

// Fonctions graphiques
// Darken a color
function darkenColor(colorStr, opacity) {
    // Defined in dygraph-utils.js
    var color = Dygraph.toRGB_(colorStr);

    color.r = Math.floor((255 + color.r) / 2);
    color.g = Math.floor((255 + color.g) / 2);
    color.b = Math.floor((255 + color.b) / 2);
    return 'rgba(' + color.r + ',' + color.g + ',' + color.b + ',' + opacity + ')';
}

function barChartPlotter(e) {
    var ctx = e.drawingContext;
    var points = e.points;
    var y_bottom = e.dygraph.toDomYCoord(0);

    ctx.fillStyle = darkenColor(e.color, 0.3);

    // Find the minimum separation between x-values.
    // This determines the bar width.
    var min_sep = Infinity;
    for (var i = 1; i < points.length; i++) {
        var sep = points[i].canvasx - points[i - 1].canvasx;
        if (sep < min_sep) min_sep = sep;
    }
    var bar_width = Math.floor(2.0 / 3 * min_sep);

    // Do the actual plotting.
    for (var i = 0; i < points.length; i++) {
        var p = points[i];
        var center_x = p.canvasx;

        ctx.fillRect(center_x - bar_width / 2, p.canvasy,
            bar_width, y_bottom - p.canvasy);

        ctx.strokeRect(center_x - bar_width / 2, p.canvasy,
            bar_width, y_bottom - p.canvasy);
    }
}

function hightlight_WE(canvas, area, g, dygraphs) {
    canvas.fillStyle = "rgba(188, 196, 197, 0.5)";

    function highlight_period(x_start, x_end) {
        var canvas_left_x = g.toDomXCoord(x_start);
        var canvas_right_x = g.toDomXCoord(x_end);
        var canvas_width = canvas_right_x - canvas_left_x;
        canvas.fillRect(canvas_left_x, area.y, canvas_width, area.h);
    }

    var min_data_x = g.getValue(0, 0);
    var max_data_x = g.getValue(g.numRows() - 1, 0);

    // get day of week
    var d = new Date(min_data_x);
    var dow = d.getUTCDay();

    var w = min_data_x;
    // starting on Sunday is a special case
    if (dow === 0) {
        highlight_period(w, w + 12 * 3600 * 1000);
    }
    // find first saturday
    while (dow != 6) {
        w += 24 * 3600 * 1000;
        d = new Date(w);
        dow = d.getUTCDay();
    }
    // shift back 1/2 day to center highlight around the point for the day
    w -= 12 * 3600 * 1000;
    while (w < max_data_x) {
        var start_x_highlight = w;
        var end_x_highlight = w + 2 * 24 * 3600 * 1000;
        // make sure we don't try to plot outside the graph
        if (start_x_highlight < min_data_x) {
            start_x_highlight = min_data_x;
        }
        if (end_x_highlight > max_data_x) {
            end_x_highlight = max_data_x;
        }
        highlight_period(start_x_highlight, end_x_highlight);
        // calculate start of highlight for next Saturday
        w += 7 * 24 * 3600 * 1000;
    }
};

// création simple de table html pour sweetALert pvtTable
function createTable(headers, data) {
    var table = '<table id="table_taux" class="w3-table-all tableOverflow-y" style="margin:5px auto;">';
    table += '<thead><tr>';
    headers.forEach((header, i) => {
        // table += '<th onclick="sortTable(' + i + ')"><div><i class="gg-sort-az"></i>&ensp;' + header + '</div></th>';
        table += '<th onclick="sortTable(' + i + ')"><div>' + header + '</div></th>';
    });
    table += '</tr></thead>';
    table += '<tbody>';
    data.forEach(row => {
        // console.log(row.length);
        table += '<tr>';
        row.forEach((cell, j) => {
            table += '<td>' + cell + '</td>';
        })
        table += '</tr>';
    });
    table += '</tbody></table>';

    return table;
}

function sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, flag, dir, switchcount = 0;
    table = document.getElementById("table_taux");
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";
    var convertToDecimal = (value) => /\,/.test(value) ? parseFloat((value.replace(/\,/, '.'))) : parseFloat(value);
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            flag = (isNaN(parseFloat(x.innerHTML)) && isNaN(parseFloat(y.innerHTML)));
            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase() && flag) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                } else if (convertToDecimal(x.innerHTML) > convertToDecimal(y.innerHTML) && !flag) {
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase() && flag) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                } else if (convertToDecimal(x.innerHTML) < convertToDecimal(y.innerHTML) && !flag) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}