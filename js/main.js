"use strict";

(function () {
    var app = {
        'routes': {
            'any': {
                'rendered': function () {
                    ;
                }
            },
            'region': {
                'rendered': function () {
                    document.getElementById('toolbars').style.display = "block"
                    document.getElementById('footer').classList.replace('footer-accueil', 'footer-chart');
                    getView(app.routeID)
                }
            },
            'dep': {
                'rendered': function () {
                    document.getElementById('toolbars').style.display = "block";
                    document.getElementById('footer').classList.replace('footer-accueil', 'footer-chart');
                    getView(app.routeID)
                }
            }
        },
        'default': 'accueil',
        'dataComplete': {},
        'postcodes': {},
        'parsexe': {
            dataFirstDay: "",
            sumDc: "",
            sumRad: ""
        },
        'indicateurs': {},
        'urgences': [],
        'testDepts': {},
        'state': "",
        'getUrl': function get(url) {
            return new Promise((resolve, reject) => {
                var req = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
                req.open('GET', url, true);
                req.onload = () => (req.status >= 200 && req.status < 400) ? resolve(req.response) : reject(Error(req.statusText));
                req.onerror = () => reject(req.status);
                req.send();
            });
        },
        'loadJS': function (url, onDone, onError) {
            if (!onDone) onDone = function () {};
            // if (!onState) onState = function() {};
            if (!onError) onError = function () {};

            // requête http
            var request = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
            request.open('GET', url, true);
            request.onload = onDone;
            // request.onreadystatechange = onState;
            request.send();
        },
        'routeChange': function () {
            app.routeID = location.hash.slice(1);
            app.route = app.routes[app.routeID];
            app.routeElem = document.getElementById(app.routeID);
            app.state = app.routeElem.dataset.state;
            app.routes['any'].rendered();
            document.getElementById('toolbars').style.display = "none";
            document.getElementById('footer').classList.replace('footer-chart', 'footer-accueil');
            if (app.route) {
                app.route.rendered();
            }
        },
        // The function to start the app.
        'init': function () {
            getPostCodes();

            var urlParSexe = 'https://www.data.gouv.fr/fr/datasets/r/63352e38-d353-4b54-bfd1-f1b3ee1cabd7';
            var urlUrgences = 'https://www.data.gouv.fr/fr/datasets/r/eceb9fb4-3ebc-4da3-828d-f5939712600a';
            var urlIndicateurs = 'https://www.data.gouv.fr/fr/datasets/r/f2d0f955-f9c4-43a8-b588-a03733a38921';
            var urlCasJour = "https://www.data.gouv.fr/fr/datasets/r/6fadff46-9efd-4c53-942a-54aca783c30c";

            // Capacité analytique de tests virologiques dans le cadre de l'épidémie COVID-19 SI-DEP
            var urlTestDepts = "https://www.data.gouv.fr/fr/datasets/r/0c230dc3-2d51-4f17-be97-aa9938564b39";
            // Taux d'incidence de l'épidémie de COVID-19 SI-DEP
            var urlCasPositifs = "https://www.data.gouv.fr/fr/datasets/r/19a91d64-3cd3-42fc-9943-d635491a4d76";
            // Données relatives aux résultats des tests virologiques COVID-19 SI-DEP
            var urlSIDEP = "https://www.data.gouv.fr/fr/datasets/r/406c6a23-e283-4300-9484-54e78c8ae675";

            getDataFiles(urlSIDEP, "SIDEP");

            getDataFiles(urlParSexe, "parsexe");
            getDataFiles(urlIndicateurs, "indicateurs");

            var routes = Object.keys(app.routes).slice(1);
            routes.forEach((route, i) => {
                createPage(route);
            });

            window.addEventListener('hashchange', function (e) {
                app.routeChange();
            });
            // if (!window.location.hash) {
            window.location.hash = "#accueil"; //app.default;
            // } else {
            //     app.routeChange();
            // }
        }
    };

    window.app = app;
})();

app.init();

function getPostCodes() {
    d3.csv('csv/insee-populations-estimation-2020-ensemble-departements.csv')
        .then(function (data) {
            data.forEach(item => {
                // console.log(item["Population municipale"].replace(/\s/gmi, ''));
                app.postcodes[item["Code département"]] = {
                    dep: item["Code département"],
                    dep_nom: item["Nom du département"],
                    region: item["Nom de la région"],
                    communes: item["Nombre de communes"],
                    population: item["Population municipale"].replace(/\s/gmi, '')

                }
            })
        });
}

function getDataFiles(url, type) {
    app.getUrl(url)
        .then((response) => {
            var delimiter = (/\,/gi).test(response) ? ',' :
                (/\;/gi).test(response) ? ";" : "\t";

            var dsv = d3.dsvFormat(delimiter);
            var data = dsv.parse(response);
            var fields = data.columns;

            if (type === "SIDEP") {
                getDataNestedSIDEP(data, fields);
            } else if (type === "indicateurs") {
                data.forEach(d => {
                    app.indicateurs[d[fields[1]]] = {
                        jour: d[fields[0]],
                        color: d[fields[4]]
                    }
                })
            } else if (type === "parsexe") {
                getDataNestedSexe(data, fields, function (dataFirstDay) {
                    getDataJour(dataFirstDay);
                });
            }
        })
        .catch((err) => {
            console.log(err);
        })
}

function getDataNestedSIDEP(data, fields, callback) {
    // filtrer pour tous âges = "0"
    data = data.filter(d => d[fields[4]] === "0");

    function cumulByArray(arr) {
        return arr.reduce((r, a) => {
            a += r[r.length - 1] || 0;
            r.push(a);
            return r;
        }, []);
    }

    var nestedData = d3.nest()
        .key(d => d[fields[0]])
        .rollup(v => {
            var cumulTestsPerDate = v.map((d, i) => {
                return {
                    date: d[fields[1]],
                    p: +d[fields[2]],
                    p_cumul: cumulByArray(v.map(d => +d[fields[2]]))[i],
                    t: +d[fields[3]],
                    t_cumul: cumulByArray(v.map(d => +d[fields[3]]))[i],
                    pop: ""
                }
            });
            // console.log(cumulTestsPerDate);
            return cumulTestsPerDate;
        })
        .entries(data);

    var dataTests = [];
    nestedData.forEach(d => {
        if (app.postcodes[d.key] !== undefined) {
            var dep = d.key;
            var region = app.postcodes[d.key].region;
            var pop = app.postcodes[dep]["population"];
            var obj = {};
            d.value.forEach(d => {
                obj = {
                    region: region,
                    dep: dep,
                    pop: +pop,
                    jour: d.date,
                    t: d.t,
                    testsCumul: d.t_cumul,
                    p: d.p,
                    casCumul: d.p_cumul,
                    cas_percent: d.p / d.t
                }
                dataTests.push(obj);
            })
        }
    });
    dataTests = dataTests.sort((a, b) => a["dep"] < b["dep"] ? -1 : a["dep"] > b["dep"] ? 1 : 0);
    app.dataComplete["dataTests"] = dataTests;
    document.getElementById("accueil").classList.replace("hide", "show");
    document.getElementById("footer").classList.replace("hide", "show");

    var labels = [...new Set(dataTests.map(d => d.region))]
        .sort((a, b) => a < b ? -1 : a > b ? 1 : 0);

    fillSelectSeries("", "accueil_regions_select", labels, "region");

    document.getElementById('accueil_title').textContent =
        'Données relatives aux résultats des tests virologiques COVID-19 (SI-DEP)';

    accueilChart(dataTests);
}

function getDataNestedSexe(data, fields, callback) {
    var dataToNest = [];
    var dataFiltered = data.filter(d => d["jour"] == "2020-03-18" && d["sexe"] == "0");

    dataFiltered.forEach(d => {
        var obj = {};
        if (app.postcodes[d[fields[0]]] !== undefined) {
            obj["region"] = app.postcodes[d[fields[0]]].region;
            obj["pop"] = +app.postcodes[d[fields[0]]].population;
            obj["dep"] = d[fields[0]];
            obj["jour"] = d[fields[2]]; // si DD/MM/YYYY : (d.jour).split("/").reverse().join("-");
            obj["hosp"] = +d[fields[3]].replace(/\s/gmi, '');
            obj["rea"] = +d[fields[4]].replace(/\s/gmi, '');
            obj["rad"] = +d[fields[5]].replace(/\s/gmi, '');
            obj["dc"] = +d[fields[6]].replace(/\s/gmi, '');
            dataToNest.push(obj);
        } else {
            // console.log(d["dep"], d.dc, d.hosp, d.rea, d.rad);
        }
    });

    app.parsexe.dataFirstDay = dataToNest;
    //.sort((a, b) => a["dep"] < b["dep"] ? -1 : a["dep"] > b["dep"] ? 1 : 0);

    var lastDay = d3.max([...new Set(data.map(d => d["jour"]))]);
    var dataLastDay = data.filter(d => d["jour"] === lastDay && d["sexe"] == "0");

    app.parsexe["sumDc"] = d3.sum(dataLastDay.map(d => +d.dc));
    app.parsexe["sumRad"] = d3.sum(dataLastDay.map(d => +d.rad));

    callback(dataToNest);
}

function getDataNestedJour(data, fields, dataFirstDay) {

    var dataToNest = [];
    data.forEach(d => {
        var obj = {};
        if (app.postcodes[d[fields[0]]] !== undefined) {
            obj["region"] = app.postcodes[d[fields[0]]].region;
            obj["pop"] = +app.postcodes[d[fields[0]]].population;
            obj["dep"] = d[fields[0]];
            obj["jour"] = d[fields[1]]; //(d.jour).split("/").reverse().join("-");
            obj["hosp"] = +d[fields[2]].replace(/\s/gmi, '');
            obj["rea"] = +d[fields[3]].replace(/\s/gmi, '');
            obj["dc"] = +d[fields[4]].replace(/\s/gmi, '');
            obj["rad"] = +d[fields[5]].replace(/\s/gmi, '');
            dataToNest.push(obj);
        } else {
            // console.log(d["dep"], d.dc, d.hosp, d.rea, d.rad);
        }
    })

    var dataComplete = dataFirstDay.concat(dataToNest)
        .sort((a, b) => a["dep"] < b["dep"] ? -1 : a["dep"] > b["dep"] ? 1 : 0);

    var allDates = [...new Set(dataComplete.map(d => d["jour"]))];
    var currentDate = allDates[allDates.length - 1];

    app.dataComplete["dataComplete"] = dataComplete;
    app.dataComplete["currentDate"] = currentDate;
    app.dataComplete["allDates"] = allDates;
    app.state = "loaded";

    getReady();
    return dataComplete;
}

function getDataJour(dataFirstDay) {
    var urlCasJour = "https://www.data.gouv.fr/fr/datasets/r/6fadff46-9efd-4c53-942a-54aca783c30c";

    app.getUrl(urlCasJour)
        .then((response) => {
            var regexDate = /\d{4}\-\d{2}\-\d{2}/gmi;

            var delimiter = (/\,/gi).test(response) ? ',' :
                (/\;/gi).test(response) ? ";" : "\t";

            var dsv = d3.dsvFormat(delimiter);
            var data = dsv.parse(response);

            var dataComplete = getDataNestedJour(data, data.columns, dataFirstDay);
        })
        .catch((err) => {
            console.log(err);
        });
}

function getView(route) {
    document.getElementById('loader_' + route).style.display = 'block';

    var unit = document.querySelector('input[name=units]:checked').value;
    var field = document.querySelector('input[name=fields]:checked').value;

    document.getElementsByName('fields').forEach(function (radio) {
        radio.onclick = function () {
            unit = document.querySelector('input[name=units]:checked').value;
            getDataChart(route, unit, radio.value);
        }
    });

    document.getElementsByName('units').forEach(function (radio) {
        radio.onclick = function () {
            field = document.querySelector('input[name=fields]:checked').value;
            getDataChart(route, radio.value, field);
        }
    });

    getDataChart(route, unit, field);
}

function getReady() {
    document.getElementById('accueil').classList.replace('hide', 'show');
    setTimeout(() => {
        if (app.state) {
            document.getElementById("loader_accueil").innerHTML = "";
            document.querySelectorAll('.pointerNone').forEach((el) => {
                el.classList.replace('pointerNone', 'pointerAuto');
            });
        }
    }, 100);
}