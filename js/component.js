"use strict";

var createPage = function (route) {
    // var auth = /auth/.test(route) ? 'Accès avec authenfication' : 'Accès sans authentification';
    // console.log(auth);

    var mainDiv = document.getElementById('main_div');

    var divPage = document.createElement('div')
    divPage.id = route;
    divPage.className = 'page';
    // divPage.style.marginTop = '60px';
    divPage.dataset.state = "";

    var titlePage = document.createElement('H3');
    titlePage.id = route + '_title';
    titlePage.className = "w3-center";

    var divTable = document.createElement('div');
    divTable.id = route + '_div';
    divTable.className = 'w3-row';
    // divTable.style.margin = "5px auto";

    var divChartContainer = document.createElement('div');
    divChartContainer.className = "w3-col s12 l9";

    var divChart = document.createElement('div');
    divChart.id = route + '_chart';
    divChart.style.width = '98%';
    divChart.style.minWIdth = '450px';

    // div selection dates et items region ou département
    var divLegendSelection = document.createElement('div');
    divLegendSelection.id = route + '_divLegendSelection';
    divLegendSelection.className = "w3-center w3-blue-grey";
    // divLegendSelection.title = "Clic long pour faire défiler";
    divLegendSelection.style.width = "100%";
    divLegendSelection.style.fontSize = "1.2em";
    divLegendSelection.style.margin = "1em auto";

    var divBarDates = document.createElement('div');
    divBarDates.className = "w3-center"; // w3-left

    var divSelectionDates = document.createElement('div');
    divSelectionDates.className = "w3-show-inline-block"; // w3-left

    var spanSelectionDatesLeft = document.createElement('span');
    spanSelectionDatesLeft.innerHTML = '<span id="' + route +
        '_left_dates_btn" class="w3-button w3-hover-blue-grey"><i class="gg-chevron-left"></i></span>';

    var spanContainerDates = document.createElement('span');
    spanContainerDates.innerHTML = '<span id="' + route +
        '_dates_container" class="w3-button w3-hover-blue-grey" style="cursor: auto"></span>';

    var spanSelectionDatesRight = document.createElement('span');
    spanSelectionDatesRight.innerHTML = '<span id="' + route +
        '_right_dates_btn" class="w3-button w3-hover-blue-grey"><i class="gg-chevron-right"></i></span>';

    // var divBarNames = document.createElement('div');
    // divBarNames.className = "w3-center"; // w3-left

    // var divSelectionNames = document.createElement('div');
    // divSelectionNames.style.width = "400px";
    // divSelectionNames.className = "w3-show-inline-block";

    // var spanSelectionNamesLeft = document.createElement('span');
    // spanSelectionNamesLeft.innerHTML = '<span id="' + route +
    //     '_left_names_btn" class="w3-button w3-left w3-hover-blue-grey"><i class="gg-chevron-left"></i></span>';

    // var spanContainerNames = document.createElement('span');
    // spanContainerNames.innerHTML = '<span id="' + route +
    //     '_names_container" class="w3-button w3-hover-blue-grey" style="cursor: auto"></span>';

    // var spanSelectionNamesRight = document.createElement('span');
    // spanSelectionNamesRight.innerHTML = '<span id="' + route +
    //     '_right_names_btn" class="w3-button w3-right w3-hover-blue-grey"><i class="gg-chevron-right"></i></span>';


    divBarDates.appendChild(spanSelectionDatesLeft);
    divBarDates.appendChild(spanContainerDates);
    divBarDates.appendChild(spanSelectionDatesRight);
    divSelectionDates.appendChild(divBarDates);
    divLegendSelection.appendChild(divSelectionDates);

    // divBarNames.appendChild(spanSelectionNamesLeft);
    // divBarNames.appendChild(spanContainerNames);
    // divBarNames.appendChild(spanSelectionNamesRight);
    // divSelectionNames.appendChild(divBarNames);
    // divLegendSelection.appendChild(divSelectionNames);

    // divPage.appendChild(divLegendSelection);
    // ==========


    // Legend container ============
    var divLegendContainer = document.createElement('div');
    divLegendContainer.id = route + '_divLegendContainer';
    divLegendContainer.className = "w3-col s12 l3";

    var divLegendPanel = document.createElement('div');
    divLegendPanel.id = route + '_divLegendPanel';
    divLegendPanel.className = "w3-panel";
    divLegendPanel.style.pointerEvents = "none";

    var divLegendPermanent = document.createElement('div');
    divLegendPermanent.id = route + '_divLegendPermanent';
    divLegendPermanent.className = "w3-panel";
    divLegendPermanent.style.pointerEvents = "none";

    var divLegendDateX = document.createElement('div');
    divLegendDateX.id = route + '_legend_dateX';
    divLegendDateX.className = "w3-panel w3-leftbar w3-border-blue-grey";
    divLegendDateX.style.position = "absolute";
    divLegendDateX.style.top = "130px";
    divLegendDateX.style.left = "120px"

    var divLegendTotal = document.createElement('div');
    divLegendTotal.id = route + '_legend_total';
    divLegendTotal.className = "w3-panel w3-leftbar w3-border-blue-grey";
    divLegendTotal.style.position = "absolute";
    divLegendTotal.style.top = "160px";
    divLegendTotal.style.left = "120px";

    var divLegendSelected = document.createElement('div');
    divLegendSelected.id = route + '_legend_selected';
    divLegendSelected.className = "w3-panel w3-leftbar w3-border-blue-grey";
    divLegendSelected.style.position = "absolute";
    divLegendSelected.style.top = "200px";
    divLegendSelected.style.left = "120px";

    var divLegendPopulation = document.createElement('div');
    divLegendPopulation.id = route + '_legend_population';
    divLegendPopulation.className = "w3-panel w3-leftbar w3-border-blue-grey";
    divLegendPopulation.style.position = "absolute";
    divLegendPopulation.style.top = "230px";
    divLegendPopulation.style.left = "120px";
    divLegendPopulation.style.display = "none";

    var divLegendRatioHosp = document.createElement('div');
    divLegendRatioHosp.id = route + '_legend_ratioHosp';
    divLegendRatioHosp.className = "w3-panel w3-leftbar w3-border-blue-grey";
    divLegendRatioHosp.style.position = "absolute";
    divLegendRatioHosp.style.top = "270px";
    divLegendRatioHosp.style.left = "120px";
    divLegendRatioHosp.style.display = "none";

    var divLegendSelect = document.createElement('select');
    divLegendSelect.id = route + '_names_select';
    divLegendSelect.className = "cov-btn-select"

    var divLegend = document.createElement('div');
    divLegend.id = route + '_legend';
    divLegend.className = "w3-panel w3-border w3-round-small";

    divLegendPermanent.appendChild(divLegendDateX);
    divLegendPermanent.appendChild(divLegendTotal);
    divLegendPanel.appendChild(divLegendSelected);
    divLegendPanel.appendChild(divLegendPopulation);
    divLegendPanel.appendChild(divLegendRatioHosp);

    divLegendContainer.appendChild(divLegendSelection);
    divLegendContainer.appendChild(divLegendSelect);
    divLegendContainer.appendChild(divLegend);
    // ======

    divLegendContainer.appendChild(divLegendPermanent);
    divLegendContainer.appendChild(divLegendPanel);
    divChartContainer.appendChild(divChart);
    divTable.appendChild(divChartContainer);
    divTable.appendChild(divLegendContainer);

    // div.appendChild(createPageButtonsBar(route));
    divPage.appendChild(titlePage)
    divPage.appendChild(createPageLoader(route));
    divPage.appendChild(divTable);

    mainDiv.appendChild(divPage);
    app.dataComplete[route] = {};
}

var createPageLoader = function (route) {
    var div = document.createElement('div')
    div.id = 'loader_' + route;
    div.className = "w3-container w3-center w3-animate-zoom loader-div";
    // div.title = "pour test";
    // div.innerHTML = 'profile ' + route;
    div.dataset.state = "";

    // var p = document.createElement('p');
    // p.innerHTML = 'Chargement des données...';

    var loader_div = document.createElement('div');
    loader_div.className = "loader";

    // div.appendChild(p);
    div.appendChild(loader_div);

    return div;
}