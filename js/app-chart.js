"use strict";

function getDataChart(route, unit, field) {

    var dataComplete = app.dataComplete["dataComplete"];

    var currentDate = app.dataComplete["currentDate"];
    var dates = app.dataComplete["allDates"];
    var dateRange = d3.extent(app.dataComplete.allDates);

    var names = [...new Set(dataComplete.map(d => d[route]))];

    // fillSelectSeries('select_dates', dates, 'dates');
    fillSelectSeries(route, '_names_select', names, "");


    var nestedData = function (dataSeries) {
        return d3.nest()
            .key(d => d["jour"])
            .key(d => d[route])
            .rollup(v => {
                var cumul = 0;
                var total = d3.sum(v.map(d => d[field]));
                var hosp = d3.sum(v.map(d => d.hosp));
                var pop = d3.sum(v.map(d => d.pop))

                cumul += hosp;
                return {
                    total: total,
                    hosp: hosp,
                    // hom: hom,
                    // fem: fem,
                    pop: pop,
                    cumul: cumul
                }
            })
            .entries(dataSeries)
    };

    var selectedUnits = {
        'nombre': "en nombre",
        'proportions': "en proportion"
    }

    var selectedFields = {
        'hosp': "Personnes nouvellement hospitalisées",
        'rea': "Personnes nouvellement en réanimation ou en soins intensifs",
        'rad': "Personnes nouvellement retournées à domicile",
        'dc': "Personnes nouvellement décédées à l'hôpital",
        'dc_ehpad': "Personnes nouvellement décédées en ehpad",
        'dc_ehpad': "Personnes nouvellement décédées (hôpital + ehpad)",
    }

    var zone = route === "region" ? "région" : route === "dep" ? "département" : "total";
    var titleGraph = selectedFields[field] + ' par ' + zone; // + ' (' + selectedUnits[unit] + ')';
    document.getElementById(route + '_title').textContent = titleGraph;


    var dataset = [];
    var population = {};
    var ratioHosp = {};
    var cumulTotalSeries = {};
    var seriesCumul = {};

    nestedData(dataComplete).forEach((d, i) => {
        var date = d.key;
        var values1 = d.values;
        var row = [];
        var rowHosp = {};
        var total, hosp, hom, fem, pop,
            percent, percentHosp;

        var cumul = 0;
        values1.forEach((d, j) => {
            var name = d.key;
            total = d.value.total;
            hosp = d.value.hosp;
            pop = d3.sum(d.value.pop);

            percent = !isNaN(total / pop) && (total / pop) !== Infinity ? total / pop : 0;
            percentHosp = !isNaN(total / hosp) && (total / hosp) !== Infinity ? total / hosp : 0;

            (unit === 'nombre') ? row.push(total): row.push(percent);

            seriesCumul[name] = total;
            rowHosp[name] = percentHosp;
            population[name] = pop;
            cumul += total;
        });
        ratioHosp[date] = rowHosp;
        row.unshift(new Date(date));
        dataset.push(row);
        cumulTotalSeries[date] = cumul;

    });

    names.unshift("jour");

    var config = {
        names: names,
        dataset: dataset,
        cumulTotalSeries: cumulTotalSeries,
        ratioHosp: ratioHosp,
        dateRange: dateRange,
        population: population,
        seriesCumulSorted: Object.entries(seriesCumul).sort((a, b) => b[1] - a[1]),
        selectedUnits: selectedUnits,
        selectedFields: selectedFields,
        populationTotale: d3.sum(Object.entries(population).map(d => d[1]))

    };

    var cumulSerie = d3.sum(config.seriesCumulSorted.map(d => d[1]));
    var cumulSeriePercent = cumulSerie / config.populationTotale;

    document.getElementById(route + '_legend_dateX').innerHTML = '<span style="font-size: 1.5em; font-weight: 600">' +
        moment(currentDate, "YYYY-MM-DD").format("dddd, DD MMMM YYYY") + '</span>';

    var cumul = unit === "nombre" ?
        'Total: ' + localeFR.format(",")(cumulSerie) + ' (France entière)' :
        'réprésente ' + localeFR.format(".2%")(cumulSeriePercent) + ' de la population (France entière)';

    document.getElementById(route + '_legend_total').innerHTML = cumul;

    document.getElementById(route + '_divLegendPermanent').style.display = "block";

    launchChart(route, config, unit, field, currentDate);
}


function launchChart(route, config, unit, field, currentDate) {
    var field = document.querySelector('input[name=fields]:checked').value;
    var scale = document.querySelector('input[name = "scales"]:checked').value;

    var names = config.names.slice(1, config.names.length - 1);
    names = names.sort((a, b) => a < b ? -1 : a > b ? 1 : 0);

    var setDateSelection = function (index) {
        var dates = app.dataComplete["allDates"];

        var date = (index !== undefined) ?
            moment(dates[index], "YYYY-MM-DD").format("dddd, DD MMMM YYYY") :
            moment(config.dateRange[1], "YYYY-MM-DD").format("dddd, DD MMMM YYYY");
        return date;
    };

    var resetNameSelection = function (index) {
        var select = document.getElementById(route + '_names_select');
        select.selectedIndex = 0;
    };

    var getZone = (dep) => {
        return (app.indicateurs[dep].color == "rouge") ?
            '<i class="gg-shape-circle-red"></i>' :
            (app.indicateurs[dep].color == "orange") ?
            '<i class="gg-shape-circle-orange"></i>' :
            '<i class="gg-shape-circle-green"></i>';
    }

    var setLabeledDataLegend = (label, y) => {
        return route === 'region' ? '<b>' + label +
            ' </b><span class="w3-round w3-blue-grey" style="padding: 2px 4px">' + y + '</span>' :
            '(<b>' + label + '):</b><span class="w3-round w3-blue-grey" style="padding: 2px 4px">' + y + '</span>';
    }

    var setLabeledData = (label, y) => {
        return '(<b>' + getZone(label) + ' ' + label + '):</b> ' + y;

    }

    document.getElementById(route + '_dates_container').textContent = setDateSelection();

    var legendFormatter = function (data) {
        var html = "";
        if (data.x == null) {
            html += route === "region" ?
                config.seriesCumulSorted
                .map(arr => '<b>' + arr[0] + ':</b> ' + arr[1]).join('<br>') + '</div>' :
                config.seriesCumulSorted
                .map(arr => setLabeledData(arr[0], arr[1])).join(' ') + '</div>';

            return html;
        }
        var date_YYYYMMDD = moment(data.xHTML, "YYYY/MM/DD HH:mm").format("YYYY-MM-DD");

        document.getElementById(route + '_legend_dateX').innerHTML =
            '<span style="font-size: 1.5em; font-weight: 600">' +
            moment(data.xHTML, "YYYY/MM/DD HH:mm").format("dddd, DD MMMM YYYY") + '</span>';

        var cumulPercent = config.cumulTotalSeries[date_YYYYMMDD] / config.populationTotale;

        var cumul = unit === "nombre" ?
            'France entière : <span class="w3-round w3-blue-grey" style="padding: 2px 5px">' + localeFR.format(",")
            (d3.sum(data.series.map(a => a.y))) + '</span>' :
            'France entière : <span class="w3-round w3-blue-grey" style="padding: 2px 5px">' + localeFR.format(
                ".2%")(cumulPercent);

        document.getElementById(route + '_legend_total').innerHTML = cumul;

        data.series.sort((a, b) => b.y - a.y);

        data.series.forEach(function (serie, i) {
            if (!serie.isVisible) return;

            var population = config.population[serie.labelHTML];

            var ratioHosp = config.ratioHosp[date_YYYYMMDD][serie.labelHTML];

            var labeledData = route !== "region" ?
                setLabeledData(serie.labelHTML, serie.yHTML) :
                serie.labelHTML + ': ' + serie.yHTML;

            var serieColor = serie.color;

            if (serie.isHighlighted) {
                if (route !== "region") {
                    document.getElementById(route + '_legend_selected').style.background =
                        serie.color.replace(/rgb/, 'rgba').replace(/\)/, ', 0.2)');

                    document.getElementById(route + '_legend_selected').innerHTML =
                        '<span style="font-size: 1.5em; font-weight: 600">' + '(' + getZone(serie.labelHTML) +
                        ' ' + serie.labelHTML + ') ' +
                        app.postcodes[serie.labelHTML].dep_nom + ': ' +
                        '<span class="w3-round w3-blue-grey" style="padding: 2px 4px">' + serie.yHTML +
                        '</span></span>';

                    document.getElementById(route + '_legend_population').innerHTML =
                        '<span>sur une population de ' + localeFR.format(",")(population) + '</span>';

                    if (field === "rea") {
                        var ratioHospHTML = ratioHosp <= 1 ? localeFR.format(".2%")(ratioHosp) :
                            '<span style="color:red">' + localeFR.format(".2%")(ratioHosp) + ' ??</span>';
                        document.getElementById(route + '_legend_ratioHosp').innerHTML =
                            '<span>' + ratioHospHTML + ' des personnes hospitalisées</span>';
                    } else {
                        document.getElementById(route + '_legend_ratioHosp').innerHTML = "";
                    }
                } else {
                    labeledData = '<b>' + labeledData + '</b>';

                    document.getElementById(route + '_legend_selected').style.background =
                        serie.color.replace(/rgb/, 'rgba').replace(/\)/, ', 0.2)');

                    document.getElementById(route + '_legend_selected').innerHTML =
                        '<span style="font-size: 1.5em; font-weight: 600">' + setLabeledDataLegend(serie.labelHTML,
                            serie.yHTML) + '</span>';

                    document.getElementById(route + '_legend_population').innerHTML =
                        '<span>sur une population de ' + localeFR.format(",")(population) + '</span>';

                    if (field === "rea") {
                        var ratioHospHTML = ratioHosp <= 1 ? localeFR.format(".2%")(ratioHosp) :
                            '<span style="color:red">' + localeFR.format(".2%")(ratioHosp) + ' ??</span>';
                        document.getElementById(route + '_legend_ratioHosp').innerHTML =
                            '<span>' + ratioHospHTML + ' des personnes hospitalisées</span>';
                    } else {
                        document.getElementById(route + '_legend_ratioHosp').innerHTML = "";
                    }
                }
            } else {
                labeledData = '<b style="opacity: 0.3">' + labeledData + '</b>';
            }
            // html += (route === "region") ?
            //     ' ' + serie.dashHTML + ' ' + labeledData + '<br>' :
            //     ' ' + serie.dashHTML + ' ' + labeledData;
            html += (route === "region") ? labeledData + '<br>' : labeledData;
        });
        html += '</div>'
        return html;
    }

    var g2 = new Dygraph(
        document.getElementById(route + '_chart'),
        config.dataset, {
            labels: config.names,
            pop: config.population,
            height: Math.round(window.innerHeight * 0.75),
            highlightCircleSize: 1,
            drawPoints: true,
            plotter: barChartPlotter,
            highlightSeriesOpts: {
                strokeWidth: 3,
                strokePattern: [20, 20],
                highlightCircleSize: 6
            },
            highlightSeriesBackgroundAlpha: 0.2,
            // title: '<span class="title-graph" style="font-size: 16px; font-weight: normal">(bandes grisées = WE)</span>',
            legend: 'always',
            labelsDiv: document.getElementById(route + '_legend'),
            legendFormatter: legendFormatter,
            yAxisLabelWidth: 200,
            ylabel: config.selectedUnits[unit],
            customBars: false,
            showRangeSelector: true,
            rangeSelectorPlotFillColor: 'white',
            rangeSelectorAlpha: route === "region" ? 0.5 : 0.1,
            rangeSelectorPlotFillGradientColor: field === 'hosp' ? "orangered" : field === 'rad' ? 'green' : field ===
                'dc' ? 'black' : 'red',
            // showRoller: true,
            logscale: scale === 'log' ? true : false,
            underlayCallback: hightlight_WE,
            axes: {
                x: {
                    axisLabelFormatter: function (d, gran, opts) {
                        return moment(d).format("DD MMM"); // A REVOIR
                    },
                },
                y: {
                    valueFormatter: function (y) {
                        return unit !== 'nombre' ? localeFR.format(".2%")(y) : formatThousands(y);
                    },
                    axisLabelFormatter: function (y) {
                        return unit !== 'nombre' ? localeFR.format(".2%")(y) : formatThousands(y);
                    },
                    pixelsPerLabel: 50,
                    axisLabelWidth: 80
                }
            },
            unhighlightCallback: function (e, x, pts, row) {
                g2.clearSelection();
                legendPermanentHideShow(false);
                legendPanelDivHideShow(false);

                document.getElementById(route + '_dates_container').textContent = setDateSelection();
                resetNameSelection();
            },
            highlightCallback: function (e, x, pts, row) {
                legendPermanentHideShow(true);
                legendPanelDivHideShow(true);

                document.getElementById(route + '_dates_container')
                    .innerHTML = '<span style="color: #607d8b">' + setDateSelection() + '</span>';
                resetNameSelection();
            }
        }
    );

    document.getElementById('loader_' + route).style.display = 'none';

    document.getElementsByName('scales').forEach(function (radio) {
        radio.onclick = function () {
            var val = radio.value === 'log' ? true : false;
            g2.updateOptions({
                logscale: val
            });
        }
    });

    var changeDateSelection = function (side) {
        var name = document.getElementById(route + '_names_select').value;

        if (name == "") {
            g2.clearSelection();
            legendPanelDivHideShow(false);
        }

        legendPermanentHideShow(true);

        var dates_container = document.getElementById(route + '_dates_container');
        var dates = app.dataComplete.allDates;
        var date = dates_container.textContent.replace(/au /, '');

        var rank = dates.map(d => moment(d, "YYYY-MM-DD").format("dddd, DD MMMM YYYY")).indexOf(date);
        var index = side === 'left' ? rank - 1 : rank + 1;

        var className = side === 'left' ? '<span class="w3-animate-right">' : '<span class="w3-animate-lrft">';

        if (index >= 0 && index < app.dataComplete.allDates.length) {
            var newDate = moment(app.dataComplete.allDates[index], "YYYY-MM-DD").format("dddd, DD MMMM YYYY");
            dates_container.innerHTML = '<span class="w3-animate-right">' + newDate + '</span>';

            g2.setSelection(index, name);

            g2.setSelection(g2.getSelection(), g2.getHighlightSeries(), true)
        }
    }

    document.getElementById(route + '_names_select').onchange = function () {
        var name = this.value;

        if (name == "") {
            g2.clearSelection();
            legendPanelDivHideShow(false);
            return;
        }
        legendPermanentHideShow(true);
        legendPanelDivHideShow(true);

        var dates_container = document.getElementById(route + '_dates_container');
        var dates = app.dataComplete.allDates;
        var date = dates_container.textContent.replace(/au /, '');
        var indexDate = dates.map(d => moment(d, "YYYY-MM-DD").format("dddd, DD MMMM YYYY")).indexOf(date);


        g2.setSelection(indexDate, name);

        g2.setSelection(g2.getSelection(), g2.getHighlightSeries(), true)

    }

    document.getElementById(route + '_left_dates_btn').onclick = function (e) {
        changeDateSelection('left');
    };
    document.getElementById(route + '_right_dates_btn').onclick = function (e) {
        changeDateSelection('right');
    };


    var legendPermanentHideShow = function (flag) {
        if (flag) {
            document.getElementById(route + '_divLegendPermanent').style.display = "block";
        } else {
            document.getElementById(route + '_divLegendPermanent').style.display = "none";
        }

    }

    var legendPanelDivHideShow = function (flag) {
        if (flag) {
            document.getElementById(route + '_divLegendPanel').style.display = "block";
        } else {
            document.getElementById(route + '_divLegendPanel').style.display = "none";
        }

    }

    function getChartSelection() {
        var sel = g2.getSelection();
        for (var i = 0; i < sel.length; i++) {
            // console.log(sel[i])
        }
    }
}

function fillSelectSeries(route, divId, options, type) {
    divId = route ? route + divId : divId;

    var select = document.getElementById(divId);
    while (select.options.length)
        select.options.remove(0);

    type = type ? type : route;
    var optionChoisir = type === "dep" ? "choisir un département..." : "choisir une région...";

    if (options && options.length) {
        var opt = document.createElement("option");
        opt.value = "";
        opt.text = optionChoisir;
        select.add(opt, select.options[0]);
        var index = options.length - 1;
        // // voir pour ajouter un paramètre "extra" pour un select sur options dates par exemple
        // options = type === 'dates' ? options.reverse() : options;

        for (var i = 0; i < options.length; i++) {
            var option = document.createElement("option");
            var optionText = type === "dep" ? '(' + options[i] + ') ' +
                app.postcodes[options[i]].dep_nom : options[i];

            option.text = optionText;
            option.value = type === 'dates' ? i : options[i];
            select.add(option, select.options[1 + i]);
            // index--;
        }
    }
}