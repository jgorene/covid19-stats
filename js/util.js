"use strict";

const isIos = /ip(hone)/i.test(navigator.userAgent);
const isIpad = /ip(ad)/i.test(navigator.userAgent);
const isAndroid = /android/i.test(navigator.userAgent);
const isMobile = isIos || isAndroid || isIpad;

moment.locale('fr')

// commons with tabulator table
var localeFR = d3.formatLocale({
    decimal: ",",
    thousands: " ",
    grouping: [3]
});

var format0dec = localeFR.format(".0f");
var format2dec = localeFR.format(".2f");
var format3dec = localeFR.format(".3f");
var formatPercent = localeFR.format(".3%");
var formatExponent = localeFR.format(".2e");
var formatThousands = localeFR.format(",");

function convertToMillions(value) {
    // console.log(value);
    // Nine Zeroes for Billions
    return Math.abs(Number(value)) >= 1.0e+9
    ? localeFR.format(".3f")(Math.round((Math.abs(value) / 1.0e+9 + 0.00001) * 1000) / 1000) + " billions"

    // Six Zeroes for Millions
    : Math.abs(Number(value)) >= 1.0e+6
    ? localeFR.format(".3f")(Math.round((Math.abs(value) / 1.0e+6 + 0.00001) * 1000) / 1000) + " millions"

    : localeFR.format(",")(Math.abs(value));
}

var fr = {
    "name": "fr",
    "options": {
        "months": [
            "janvier",
            "février",
            "mars",
            "avril",
            "mai",
            "juin",
            "juillet",
            "août",
            "septembre",
            "octobre",
            "novembre",
            "décembre"
        ],
        "shortMonths": [
            "janv.",
            "févr.",
            "mars",
            "avr.",
            "mai",
            "juin",
            "juill.",
            "août",
            "sept.",
            "oct.",
            "nov.",
            "déc."
        ],
        "days": [
            "dimanche",
            "lundi",
            "mardi",
            "mercredi",
            "jeudi",
            "vendredi",
            "samedi"
        ],
        "shortDays": ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
        "toolbar": {
            "exportToSVG": "format SVG",
            "exportToPNG": "format PNG",
            "exportToCSV": "format CSV",
            "menu": "Menu",
            "selection": "Sélection",
            "selectionZoom": "Sélection et zoom",
            "zoomIn": "Zoomer",
            "zoomOut": "Dézoomer",
            "pan": "Navigation",
            "reset": "Réinitialiser le zoom"
        }
    }
}

// ferme le menu sur ipad à la sélection d'un item
function closeMenulist(el) {
    var mediaQuerie = window.matchMedia("(min-width: 49em)");
    // console.log(mediaQuerie, el);

    var setClick = () => {
        if (el !== undefined) {
            el.click();
        }
    }
    if (mediaQuerie !== undefined && mediaQuerie.matches) { // If media query matches
        var event = new Event("touchstart");
        el.addEventListener("touchstart", setClick, false);
        el.dispatchEvent(event);
        el.removeEventListener("touchstart", setClick, false);
    } else {
        setClick();
    }
}

document.getElementById("menulist").addEventListener("click", function (e) {
    var children = this.childNodes;
    if (e.target) {
        // replie le menu hamburger après choix item
        closeMenulist(document.getElementById('menu-btn')); // Call listener function at run time

        document.querySelectorAll('.page').forEach((el) => {
            var page = document.getElementById(el.id);
            // window.getComputedStyle(page, null).getPropertyValue("opacity")
            if (page.offsetHeight === 0) {
                children.forEach(child => {
                    if (child.nodeName === "LI") {
                        var classDrop = child.classList.contains('dropdown');
                        var child_a = child.querySelector("a");
                        var child_div = child.querySelector("div");
                        if (e.target.getAttribute('href') !== child_a.getAttribute("href")) {
                            child_a.classList.remove('menu-active');
                            if (classDrop) {
                                if (e.target.getAttribute('href') !== child_div.querySelector("a").getAttribute("href"))
                                    child_a.classList.remove('menu-active');

                                child_div.childNodes.forEach(child => {
                                    if (child.nodeName === "A")
                                        if (e.target.getAttribute('href') !== child.getAttribute("href"))
                                            child.classList.remove('menu-active');
                                });
                            }
                        }
                    }
                })
            } else {
                if (!e.target.classList.contains('dropdown'))
                    e.target.classList.add('menu-active');
            }
        })
    }
})